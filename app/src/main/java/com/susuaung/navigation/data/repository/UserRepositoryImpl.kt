package com.susuaung.navigation.data.repository

import com.susuaung.navigation.domain.model.AppUser
import com.susuaung.navigation.domain.preference.PreferenceManager
import com.susuaung.navigation.domain.repository.UserRepository
import io.reactivex.Single

class UserRepositoryImpl constructor(
    private val preferenceManager: PreferenceManager
) : UserRepository {
    override fun getUser(): AppUser? {
        return preferenceManager.getUser()
    }

    override fun updateUser(user: AppUser?) {
        if (user == null)
            preferenceManager.removeUser()
        else
            preferenceManager.saveUser(user)
    }

    override fun logout(): Single<Any> {
        preferenceManager.logout()
        return Single.just(Any())
    }

    override fun updateUserProfileImagePath(path: String) {
        preferenceManager.saveUserProfilePhoto(path)
    }

    override fun getProfileImagePath(): String? {
        return preferenceManager.getUserProfilePath()
    }

    override fun isFirstTimeAppLaunch(): Boolean {
        return preferenceManager.isFirstTimeAppLaunch()
    }
}