package com.susuaung.navigation.data.repository

import com.susuaung.navigation.domain.model.CartItem
import com.susuaung.navigation.domain.repository.ShoppingCartRepository
import io.reactivex.Observable

class ShoppingCartRepositoryImpl : ShoppingCartRepository {

    override fun retrieveItemsInsideCart(): Observable<List<CartItem>?> {
        return Observable.just(null)
    }

    override fun updateItemInsideCart(itemToUpdate: CartItem) {

    }

    override fun removeItemFromCart(itemToRemove: CartItem) {

    }
}