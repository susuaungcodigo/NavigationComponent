package com.susuaung.navigation.data.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.os.Bundle
import androidx.core.app.NotificationCompat
import androidx.navigation.NavDeepLinkBuilder
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.susuaung.navigation.R
import com.susuaung.navigation.app.feature.home.DrawerActivity

class AppFirebseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        sendNotification(message)
    }

    private fun sendNotification(remoteMessage: RemoteMessage) {

        val args = Bundle()
        args.putString("notification_id", remoteMessage.data["notification_id"].toString())
        //contentIntent.putExtras(args)

        /*val pendingIntent =
            PendingIntent.getActivity(this, 0, contentIntent, PendingIntent.FLAG_UPDATE_CURRENT)*/

        val pendingIntent = NavDeepLinkBuilder(this)
            .setGraph(R.navigation.nav_graph)
            .setDestination(R.id.dest_noti_details)
            .setArguments(args)
            .createPendingIntent()

        /*val deeplink = Navigation.findNavController().createDeepLink()
            .setDestination(R.id.nav_deeplink)
            .setArguments(args)
            .createPendingIntent()*/

        val smallIcon: Int = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            R.drawable.ic_noti_white
        } else {
            R.drawable.ic_noti_accent
        }

        val largeIcon = BitmapFactory.decodeResource(resources, R.drawable.ic_person)

        val notificationBuilder =
            NotificationCompat.Builder(applicationContext, "navigation_channel_1")
                .setContentIntent(pendingIntent)
                .setContentTitle(remoteMessage.notification!!.title)
                .setContentText(remoteMessage.notification!!.body)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setStyle(NotificationCompat.BigTextStyle())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setSmallIcon(smallIcon)
                .setLargeIcon(largeIcon)
                .setAutoCancel(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.color = applicationContext.resources.getColor(R.color.colorPrimary)
        }

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelId = "navigation_channel_1"
            val channel = NotificationChannel(
                channelId,
                "Navigation Channel One", NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationManager.createNotificationChannel(channel)
            notificationBuilder.setChannelId(channelId)
        }

        notificationManager.notify(0, notificationBuilder.build())
    }
}