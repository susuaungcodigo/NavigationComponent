package com.susuaung.navigation.data.repository

import com.google.firebase.firestore.FirebaseFirestore
import com.susuaung.navigation.data.util.Constants
import com.susuaung.navigation.domain.exception.LoginException
import com.susuaung.navigation.domain.model.AppUser
import com.susuaung.navigation.domain.preference.PreferenceManager
import com.susuaung.navigation.domain.repository.AuthenticationRepository
import io.reactivex.Observable

class AuthenticationRepositoryImpl constructor(
    private val firebaseFirestore: FirebaseFirestore,
    private val preferenceManager: PreferenceManager
) :
    AuthenticationRepository {

    override fun login(appUser: AppUser): Observable<AppUser> {
        return Observable.create<AppUser> { emitter ->
            firebaseFirestore.collection(Constants.COLLECTION_USERS)
                .get()
                .addOnSuccessListener { result ->
                    var found = false
                    if (!result.isEmpty) {
                        for (document in result) {
                            val remoteUser = document.toObject(AppUser::class.java)
                            if (appUser.email == remoteUser.email && appUser.password == remoteUser.password) {
                                found = true
                                preferenceManager.saveUser(remoteUser)
                                emitter.onNext(remoteUser)
                                break
                            }
                        }
                        if (!found)
                            emitter.onError(LoginException.IncorrectEmailOrPasswordException)
                    } else {
                        emitter.onError(LoginException.IncorrectEmailOrPasswordException)
                    }
                }.addOnFailureListener {
                    emitter.onError(LoginException.LoginFailedException)
                }
        }
    }
}