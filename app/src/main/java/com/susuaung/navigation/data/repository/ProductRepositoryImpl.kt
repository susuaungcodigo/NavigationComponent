package com.susuaung.navigation.data.repository

import com.google.firebase.firestore.FirebaseFirestore
import com.susuaung.navigation.data.util.Constants
import com.susuaung.navigation.domain.exception.CartException
import com.susuaung.navigation.domain.exception.ProductDetailsException
import com.susuaung.navigation.domain.exception.ProductListException
import com.susuaung.navigation.domain.model.CartItem
import com.susuaung.navigation.domain.model.Product
import com.susuaung.navigation.domain.repository.ProductRepository
import io.reactivex.Observable

class ProductRepositoryImpl constructor(
    private val firebaseFirestore: FirebaseFirestore
) : ProductRepository {

    override fun getPopularProducts(): Observable<List<Product>?> {
        return getProducts(Constants.COLLECTION_POPULAR_PRODUCTS)
    }

    override fun getSuggestionProducts(): Observable<List<Product>?> {
        return getProducts(Constants.COLLECTION_SUGGESTION_PRODUCTS)
    }

    override fun getLatestProducts(): Observable<List<Product>?> {
        return getProducts(Constants.COLLECTION_LATEST_PRODUCTS)
    }

    private fun getProducts(collectionName: String): Observable<List<Product>?> {
        return Observable.create { emitter ->
            firebaseFirestore.collection(collectionName)
                .get()
                .addOnSuccessListener { result ->
                    val products = result.toObjects(Product::class.java)
                    emitter.onNext(products)
                }.addOnFailureListener {
                    emitter.onError(ProductListException.RetrieveProductListException)
                }
        }
    }

    override fun getProductDetailsWithCategory(
        productId: Int,
        category: String
    ): Observable<Product?> {
        return Observable.create { emitter ->
            val productsRef = firebaseFirestore.collection(category)

            val prod = productsRef.whereEqualTo("id", productId)
            prod.get().addOnSuccessListener { result ->
                if (!result.isEmpty) {
                    val product = result.documents[0].toObject(Product::class.java)
                    if (product != null)
                        emitter.onNext(product)
                    else
                        emitter.onError(ProductDetailsException.RetrieveProductDetailsException)
                } else {
                    emitter.onError(ProductDetailsException.RetrieveProductDetailsException)
                }
            }.addOnFailureListener {
                emitter.onError(ProductDetailsException.RetrieveProductDetailsException)
            }
        }
    }

    override fun addOrUpdateProductToCart(cartItem: CartItem): Observable<Boolean> {
        return Observable.create { emitter ->
            firebaseFirestore.collection(Constants.COLLECTION_CART_ITEMS)
                .document("CartItem${cartItem.category}${cartItem.product?.id}")
                .set(cartItem)
                .addOnSuccessListener {
                    emitter.onNext(true)
                }.addOnFailureListener {
                    emitter.onError(CartException.AddItemToCartException)
                }
        }
    }

    override fun getCartItems(userId: String): Observable<List<CartItem>?> {
        return Observable.create<List<CartItem>> { emitter ->
            val listenerRegistration = firebaseFirestore.collection(Constants.COLLECTION_CART_ITEMS)
                .whereEqualTo("userId", userId)
                .addSnapshotListener { querySnapshot, exception ->
                    when {
                        exception != null -> emitter.onError(exception)
                        querySnapshot != null -> {
                            val items = querySnapshot.toObjects(CartItem::class.java)
                            emitter.onNext(items)
                        }
                        else -> emitter.onError(CartException.RetrieveCartItemsException)
                    }
                }
            emitter.setCancellable { listenerRegistration.remove() } // important to prevent memory leak
        }
    }

    override fun deleteItemFromCart(cartItem: CartItem): Observable<Boolean> {
        return Observable.create { emitter ->
            firebaseFirestore.collection(Constants.COLLECTION_CART_ITEMS)
                .whereEqualTo("product.id", cartItem.product?.id)
                .whereEqualTo("category", cartItem.category)
                .get()
                .addOnSuccessListener { result ->
                    result.forEach { doc ->
                        doc.reference.delete().addOnSuccessListener {
                            emitter.onNext(true)
                        }.addOnFailureListener {
                            emitter.onError(CartException.RemoveCartItemException)
                        }
                    }
                }.addOnFailureListener {
                    emitter.onError(CartException.RemoveCartItemException)
                }
        }
    }
}