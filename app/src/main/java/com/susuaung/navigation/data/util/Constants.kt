package com.susuaung.navigation.data.util

object Constants {
    const val COLLECTION_LATEST_PRODUCTS = "latest_products"
    const val COLLECTION_POPULAR_PRODUCTS = "popular_products"
    const val COLLECTION_SUGGESTION_PRODUCTS = "suggestion_products"
    const val COLLECTION_USERS = "users"
    const val COLLECTION_CART_ITEMS = "cart_items"
}