package com.susuaung.navigation.data.preference

import android.content.Context
import com.google.gson.Gson
import com.susuaung.navigation.data.util.SafeGuarder
import com.susuaung.navigation.domain.model.AppUser
import com.susuaung.navigation.domain.preference.PreferenceManager
import io.reactivex.subjects.PublishSubject

object PrefKey {
    const val IV_SECRET = "iv_secret"
    const val USER = "app_user"
    const val USER_PROFILE_IMAGE_PATH = "user_profile_image_path"
    const val IS_FIRST_TIME_APP_LAUNCH = "is_first_time_app_launch"
}

class PreferenceManagerImpl constructor(
    context: Context,
    private val safeGuarder: SafeGuarder,
    private val alias: String
) : PreferenceManager {
    private val gson = Gson()
    private val userSubject = PublishSubject.create<AppUser>()

    private val prefs = context.getSharedPreferences(
        "Prefs",
        Context.MODE_PRIVATE
    )

    override fun saveUser(user: AppUser) {
        val jsonUser = gson.toJson(user, AppUser::class.java)
        try {
            safeGuarder.deleteSecretKey(alias)
            prefs.edit().putString(
                PrefKey.USER, String(
                    safeGuarder.encryptText(
                        alias,
                        jsonUser
                    ), Charsets.ISO_8859_1
                )
            )
                .apply()
            safeGuarder.iv?.let { saveIv(it) }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        userSubject.onNext(user)
    }

    override fun getUser(): AppUser? {
        val jsonMember = prefs.getString(PrefKey.USER, "")

        return if (!jsonMember.isNullOrBlank()) {
            try {
                val member = gson.fromJson(
                    safeGuarder.decryptData(
                        alias = alias,
                        encryptedData = jsonMember.toByteArray(Charsets.ISO_8859_1),
                        encryptionIv = getIv()
                    ), AppUser::class.java
                )
                if (member.id.isBlank()) {
                    null
                } else {
                    member
                }
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        } else {
            null
        }
    }

    override fun removeUser() {
        prefs.edit().putString(PrefKey.USER, "")
            .apply()
        userSubject.onNext(AppUser())
    }

    override fun logout() {
        removeUser()
        prefs.edit().clear().apply()
    }

    override fun saveUserProfilePhoto(path: String) {
        prefs.edit().putString(PrefKey.USER_PROFILE_IMAGE_PATH, path).apply()
    }

    override fun getUserProfilePath(): String? {
        return prefs.getString(PrefKey.USER_PROFILE_IMAGE_PATH, "")
    }

    private fun saveIv(iv: ByteArray) {
        prefs.edit().putString(PrefKey.IV_SECRET, String(iv, Charsets.ISO_8859_1)).apply()
    }

    private fun getIv(): ByteArray {
        return prefs.getString(PrefKey.IV_SECRET, "")?.toByteArray(Charsets.ISO_8859_1)
            ?: ByteArray(1)
    }

    override fun saveFirstTimeAppLaunch(isFirstTime: Boolean) {
        prefs.edit().putBoolean(PrefKey.IS_FIRST_TIME_APP_LAUNCH, isFirstTime).apply()
    }

    override fun isFirstTimeAppLaunch(): Boolean {
        return prefs.getBoolean(PrefKey.IS_FIRST_TIME_APP_LAUNCH, true)
    }
}