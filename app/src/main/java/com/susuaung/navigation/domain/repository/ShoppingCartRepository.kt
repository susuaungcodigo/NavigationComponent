package com.susuaung.navigation.domain.repository

import com.susuaung.navigation.domain.model.CartItem
import io.reactivex.Observable

interface ShoppingCartRepository {

    fun retrieveItemsInsideCart(): Observable<List<CartItem>?>

    fun updateItemInsideCart(itemToUpdate: CartItem)

    fun removeItemFromCart(itemToRemove: CartItem)
}