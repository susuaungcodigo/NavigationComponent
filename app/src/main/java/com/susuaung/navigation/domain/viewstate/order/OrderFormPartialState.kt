package com.susuaung.navigation.domain.viewstate.order

import com.susuaung.navigation.domain.model.CartItem

sealed class OrderFormPartialState {

    abstract fun reduce(oldState: OrderFormViewState): OrderFormViewState

    object LoadingOrderItems : OrderFormPartialState() {
        override fun reduce(oldState: OrderFormViewState): OrderFormViewState {
            return oldState.copy(
                loadingItems = true,
                isFetchingOrderDone = false
            )
        }
    }

    data class OrderItems(val items: List<CartItem>?) : OrderFormPartialState() {
        override fun reduce(oldState: OrderFormViewState): OrderFormViewState {
            return oldState.copy(
                loadingItems = false,
                orderItems = items,
                getUserError = null,
                isFetchingOrderDone = true
            )
        }
    }

    data class GetItemsError(val error: Throwable) : OrderFormPartialState() {
        override fun reduce(oldState: OrderFormViewState): OrderFormViewState {
            return oldState.copy(
                getItemsError = error,
                loadingItems = false,
                orderItems = null,
                isFetchingOrderDone = true
            )
        }
    }
}