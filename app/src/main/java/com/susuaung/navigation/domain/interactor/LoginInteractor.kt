package com.susuaung.navigation.domain.interactor

import com.codigo.glory.domain.executor.BackgroundThread
import com.codigo.glory.domain.executor.PostExecutionThread
import com.susuaung.navigation.domain.model.result.Result
import com.susuaung.navigation.domain.model.AppUser
import com.susuaung.navigation.domain.repository.AuthenticationRepository
import com.susuaung.navigation.domain.validator.EmailValidator
import com.susuaung.navigation.domain.validator.PasswordValidator
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class LoginInteractor constructor(
    private val emailValidator: EmailValidator,
    private val passwordValidator: PasswordValidator,
    private val authenticationRepository: AuthenticationRepository,
    mainThread: PostExecutionThread,
    backgroundThread: BackgroundThread
) : BaseInteractor(mainThread, backgroundThread) {

    fun login(appUser: AppUser): Observable<Result<AppUser>> {
        return Observable.defer {
            val emailResult = validateEmail(appUser.email)
            if (emailResult is Result.Error) {
                Observable.just(Result.Error(emailResult.exception))
            } else {
                val passwordResult = validatePassword(appUser.password)
                if (passwordResult is Result.Error) {
                    Observable.just(Result.Error(passwordResult.exception))
                } else {
                    loginToServer(appUser)
                }
            }
        }.delay(1000, TimeUnit.MILLISECONDS)
            .startWith(Result.Loading)
            .onErrorReturn { Result.Error(it) }
            .subscribeOn(backgroundThread.getScheduler())
            .observeOn(mainThread.getScheduler())
    }

    private fun loginToServer(appUser: AppUser): Observable<Result<AppUser>> {
        return authenticationRepository.login(appUser).map { Result.Success(appUser) }
    }

    private fun validateEmail(email: String?): Result<Any> {
        val emailException = emailValidator.validate(email)
        return if (emailException != null) {
            Result.Error(emailException)
        } else {
            Result.Success(Any())
        }
    }

    private fun validatePassword(password: String?): Result<Any> {
        val passwordException = passwordValidator.validate(password)
        return if (passwordException != null) {
            Result.Error(passwordException)
        } else {
            Result.Success(Any())
        }
    }
}