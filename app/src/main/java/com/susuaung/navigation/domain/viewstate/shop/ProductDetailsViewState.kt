package com.susuaung.navigation.domain.viewstate.shop

import com.susuaung.navigation.domain.model.AppUser
import com.susuaung.navigation.domain.model.Product

data class ProductDetailsViewState(
    val loading: Boolean = false,
    val productDetailsError: Throwable? = null,
    val product: Product? = null,
    val isLoggedIn: Boolean = false,
    val isCheckingDone: Boolean = false,
    val addItemToCartResult: Boolean = false,
    val user: AppUser? = null,
    val getUserError: Throwable? = null,
    val isFetchingUserDone: Boolean = false
)