package com.susuaung.navigation.domain.viewstate.settings

data class SettingsViewState(
    val isLoggedIn: Boolean = false,
    val loggedOut: Boolean = false,
    val logoutError: Throwable? = null,
    val loggingOut: Boolean = false
)