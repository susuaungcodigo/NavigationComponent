package com.susuaung.navigation.domain.interactor

import com.codigo.glory.domain.executor.BackgroundThread
import com.codigo.glory.domain.executor.PostExecutionThread
import com.susuaung.navigation.domain.model.result.Result
import com.susuaung.navigation.domain.model.AppUser
import com.susuaung.navigation.domain.model.CartItem
import com.susuaung.navigation.domain.repository.ProductRepository
import com.susuaung.navigation.domain.repository.UserRepository
import io.reactivex.Observable
import io.reactivex.Single

class ShoppingCartInteractor(
    private val productRepository: ProductRepository,
    private val userRepository: UserRepository,
    mainThread: PostExecutionThread,
    backgroundThread: BackgroundThread
) : BaseInteractor(mainThread, backgroundThread) {

    fun getUserForNavigation(): Single<Result<AppUser>> {
        return Single.fromCallable {
            val user = userRepository.getUser()
            if (user == null) {
                Result.Error(Throwable("No User found."))
            } else {
                Result.Success(user)
            }
        }
            .subscribeOn(backgroundThread.getScheduler())
            .observeOn(mainThread.getScheduler())
    }

    fun getCartItems(): Observable<Result<List<CartItem>?>> {
        return Observable.defer {
            val user = userRepository.getUser()
            if (user == null) {
                Observable.just(Result.Success(emptyList()))
            } else {
                productRepository.getCartItems(user.id).map { Result.Success(it) }
            }
        }
    }

    fun removeItemFromCart(cartItem: CartItem): Observable<Result<Boolean>> {
        return productRepository.deleteItemFromCart(cartItem).map {
            Result.Success(it)
        }
    }
}