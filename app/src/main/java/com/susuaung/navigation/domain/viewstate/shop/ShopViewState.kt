package com.susuaung.navigation.domain.viewstate.shop

import com.susuaung.navigation.domain.model.Product

data class ShopViewState(
    val loadingPopular: Boolean = false,
    val loadingLatest: Boolean = false,
    val loadingSuggestion: Boolean = false,
    val latestProducts: List<Product>? = null,
    val popularProducts: List<Product>? = null,
    val suggestionProducts: List<Product>? = null,
    val latestProductsError: Throwable? = null,
    val popularProductsError: Throwable? = null,
    val suggestionProductsError: Throwable? = null
)