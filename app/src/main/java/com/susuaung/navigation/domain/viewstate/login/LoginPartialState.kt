package com.susuaung.navigation.domain.viewstate.login

import com.susuaung.navigation.domain.model.AppUser


sealed class LoginPartialState {
    abstract fun reduce(oldState: LoginViewState): LoginViewState

    data class LoginSuccess(val appUser: AppUser?) : LoginPartialState() {
        override fun reduce(oldState: LoginViewState): LoginViewState {
            return oldState.copy(
                loading = false,
                loginSuccess = appUser,
                loginError = null
            )
        }
    }

    data class LoginError(val error: Throwable) : LoginPartialState() {
        override fun reduce(oldState: LoginViewState): LoginViewState {
            return oldState.copy(
                loading = false,
                loginError = error,
                loginSuccess = null
            )
        }
    }

    object Loading : LoginPartialState() {
        override fun reduce(oldState: LoginViewState): LoginViewState {
            return oldState.copy(
                loading = true,
                loginError = null,
                loginSuccess = null
            )
        }
    }
}