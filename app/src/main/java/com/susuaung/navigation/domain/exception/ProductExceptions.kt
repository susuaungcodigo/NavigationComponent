package com.susuaung.navigation.domain.exception

sealed class ProductDetailsException : Exception() {

    object RetrieveProductDetailsException : ProductDetailsException()
}

sealed class ProductListException : Exception() {

    object RetrieveProductListException : ProductListException()
}

sealed class CartException : Exception() {

    object RetrieveCartItemsException : CartException()

    object RemoveCartItemException : CartException()

    object AddItemToCartException : CartException()
}