package com.susuaung.navigation.domain.validator

import com.susuaung.navigation.domain.exception.PasswordInputException


class PasswordValidator {
    fun validate(password: CharSequence?): PasswordInputException? {
        var passwordError: PasswordInputException? = null
        if (password.isNullOrBlank()) {
            passwordError = PasswordInputException.EmptyPasswordException
        }

        if (!password.isNullOrBlank() && password.length < 6) {
            passwordError = PasswordInputException.PasswordLengthLessThanSixException
        }

        if (password != null && password.contains(" ")) {
            passwordError = PasswordInputException.PasswordIncludesSpaceException
        }
        return passwordError
    }
}
