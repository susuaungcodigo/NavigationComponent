package com.susuaung.navigation.domain.interactor

import com.codigo.glory.domain.executor.BackgroundThread
import com.codigo.glory.domain.executor.PostExecutionThread
import com.susuaung.navigation.domain.model.result.Result
import com.susuaung.navigation.domain.model.Product
import com.susuaung.navigation.domain.repository.ProductRepository
import io.reactivex.Observable

class ShopInteractor constructor(
    private val productRepository: ProductRepository,
    backgroundThread: BackgroundThread,
    mainThread: PostExecutionThread
) : BaseInteractor(mainThread, backgroundThread) {

    fun getPopularProducts(): Observable<Result<List<Product>?>> {
        return productRepository.getPopularProducts().map {
            Result.Success(it)
        }
    }

    fun getLatestProducts(): Observable<Result<List<Product>?>> {
        return productRepository.getLatestProducts().map {
            Result.Success(it)
        }
    }

    fun getSuggestionProducts(): Observable<Result<List<Product>?>> {
        return productRepository.getSuggestionProducts().map {
            Result.Success(it)
        }
    }
}