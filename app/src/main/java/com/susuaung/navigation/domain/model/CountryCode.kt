package com.susuaung.navigation.domain.model

data class CountryCode(
    val name: String = "",
    val dial_code: String = "",
    val code: String = ""
)
