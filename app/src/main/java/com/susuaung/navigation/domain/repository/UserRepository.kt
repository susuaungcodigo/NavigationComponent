package com.susuaung.navigation.domain.repository

import com.susuaung.navigation.domain.model.AppUser
import io.reactivex.Single

interface UserRepository {

    fun getUser(): AppUser?

    fun updateUser(user: AppUser?)

    fun logout(): Single<Any>

    fun updateUserProfileImagePath(path: String)

    fun getProfileImagePath(): String?

    fun isFirstTimeAppLaunch(): Boolean
}