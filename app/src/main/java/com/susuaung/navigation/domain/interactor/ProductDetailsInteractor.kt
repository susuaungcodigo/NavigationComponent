package com.susuaung.navigation.domain.interactor

import com.codigo.glory.domain.executor.BackgroundThread
import com.codigo.glory.domain.executor.PostExecutionThread
import com.susuaung.navigation.domain.model.AppUser
import com.susuaung.navigation.domain.model.CartItem
import com.susuaung.navigation.domain.model.Product
import com.susuaung.navigation.domain.model.result.Result
import com.susuaung.navigation.domain.repository.ProductRepository
import com.susuaung.navigation.domain.repository.UserRepository
import io.reactivex.Observable

class ProductDetailsInteractor constructor(
    private val productRepository: ProductRepository,
    private val userRepository: UserRepository,
    mainThread: PostExecutionThread,
    backgroundThread: BackgroundThread
) : BaseInteractor(mainThread, backgroundThread) {

    fun getProductDetailsWithCategory(
        productId: Int,
        category: String
    ): Observable<Result<Product?>> {
        return productRepository.getProductDetailsWithCategory(productId, category).map {
            Result.Success(it)
        }
    }

    fun isLoggedIn(): Observable<Result<Boolean>> {
        return Observable.fromCallable {
            Result.Success(userRepository.getUser() != null)
        }
    }

    fun addItemToCart(cartItem: CartItem): Observable<Result<Boolean>> {
        return Observable.defer {
            productRepository.addOrUpdateProductToCart(cartItem).map {
                Result.Success(it)
            }
        }
    }

    fun getUser(): Observable<Result<AppUser>> {
        return Observable.fromCallable {
            val user = userRepository.getUser()
            if (user == null) {
                Result.Error(Throwable("No User found."))
            } else {
                Result.Success(user)
            }
        }
            .subscribeOn(backgroundThread.getScheduler())
            .observeOn(mainThread.getScheduler())
    }
}