package com.susuaung.navigation.domain.viewstate.profile

import com.susuaung.navigation.domain.model.AppUser

sealed class ProfilePartialState {

    abstract fun reduce(oldState: ProfileViewState): ProfileViewState

    data class UserInfo(val appUser: AppUser?) : ProfilePartialState() {
        override fun reduce(oldState: ProfileViewState): ProfileViewState {
            return oldState.copy(
                userInfo = appUser,
                loading = false,
                getUserInfoError = null,
                isFetchingDone = true
            )
        }
    }

    data class ProfileImagePath(val path: String?) : ProfilePartialState() {
        override fun reduce(oldState: ProfileViewState): ProfileViewState {
            return oldState.copy(
                loading = false,
                profileImagePath = path
            )
        }
    }

    data class GetUserInfoError(val error: Throwable) : ProfilePartialState() {
        override fun reduce(oldState: ProfileViewState): ProfileViewState {
            return oldState.copy(
                userInfo = null,
                loading = false,
                getUserInfoError = error
            )
        }
    }

    object Loading : ProfilePartialState() {
        override fun reduce(oldState: ProfileViewState): ProfileViewState {
            return oldState.copy(
                userInfo = null,
                loading = true,
                getUserInfoError = null
            )
        }
    }
}