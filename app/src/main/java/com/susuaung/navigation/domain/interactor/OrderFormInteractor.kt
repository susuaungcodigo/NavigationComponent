package com.susuaung.navigation.domain.interactor

import com.codigo.glory.domain.executor.BackgroundThread
import com.codigo.glory.domain.executor.PostExecutionThread
import com.susuaung.navigation.domain.model.result.Result
import com.susuaung.navigation.domain.model.AppUser
import com.susuaung.navigation.domain.model.CartItem
import com.susuaung.navigation.domain.repository.ProductRepository
import com.susuaung.navigation.domain.repository.UserRepository
import io.reactivex.Observable

class OrderFormInteractor(
    private val productRepository: ProductRepository,
    private val userRepository: UserRepository,
    mainThred: PostExecutionThread,
    backgroundThread: BackgroundThread
) : BaseInteractor(mainThred, backgroundThread) {

    fun getUser(): Observable<Result<AppUser>> {
        return Observable.fromCallable {
            val user = userRepository.getUser()
            if (user == null) {
                Result.Error(Throwable("No User found."))
            } else {
                Result.Success(user)
            }
        }
            .subscribeOn(backgroundThread.getScheduler())
            .observeOn(mainThread.getScheduler())
    }

    fun getOrderItems(userId: String): Observable<Result<List<CartItem>?>> {
        return productRepository.getCartItems(userId).map {
            Result.Success(it)
        }
    }

    fun increaseItemCount() {

    }

    fun decreaseItemCount() {

    }

    fun postOrder() {

    }
}