package com.susuaung.navigation.domain.viewstate.login

import com.susuaung.navigation.domain.model.AppUser

data class LoginViewState(
    val loginSuccess: AppUser? = null,
    val loginError: Throwable? = null,
    val loading: Boolean = false
)