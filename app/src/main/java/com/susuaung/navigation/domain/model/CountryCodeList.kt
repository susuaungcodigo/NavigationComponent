package com.susuaung.navigation.domain.model

data class CountryCodeList(
    val countrycodeList: List<CountryCode>
)