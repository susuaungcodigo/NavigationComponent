package com.susuaung.navigation.domain.interactor

import com.codigo.glory.domain.executor.BackgroundThread
import com.codigo.glory.domain.executor.PostExecutionThread
import com.susuaung.navigation.domain.model.result.Result
import com.susuaung.navigation.domain.repository.UserRepository
import io.reactivex.Observable

class SplashInteractor constructor(
    private val userRepository: UserRepository,
    mainThread: PostExecutionThread,
    backgroundThread: BackgroundThread
) : BaseInteractor(mainThread, backgroundThread) {

    fun isFirstTimeAppLaunch(): Observable<Result.Success<Boolean>> {
        return Observable.just(Result.Success(userRepository.isFirstTimeAppLaunch()))
    }
}