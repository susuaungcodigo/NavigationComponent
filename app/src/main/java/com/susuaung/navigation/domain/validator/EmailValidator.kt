package com.susuaung.navigation.domain.validator

import com.susuaung.navigation.domain.exception.EmailInputException
import java.util.regex.Pattern

class EmailValidator {

    fun validate(email: CharSequence?): EmailInputException? {
        var emailError: EmailInputException? = null
        if (email.isNullOrBlank()) {
            emailError = EmailInputException.EmptyEmailException
        } else {
            val valid = Pattern.compile(
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]|[\\w-]{2,}))@" +
                        "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?" +
                        "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\." +
                        "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?" +
                        "[0-9]{1,2}|25[0-5]|2[0-4][0-9]))|" +
                        "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
            ).matcher(email).matches()

            if (!valid) {
                emailError = EmailInputException.InvalidEmailException
            }
        }
        return emailError
    }
}
