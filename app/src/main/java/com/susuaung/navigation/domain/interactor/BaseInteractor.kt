package com.susuaung.navigation.domain.interactor

import com.codigo.glory.domain.executor.BackgroundThread
import com.codigo.glory.domain.executor.PostExecutionThread

open class BaseInteractor(
    val mainThread: PostExecutionThread,
    val backgroundThread: BackgroundThread
)
