package com.susuaung.navigation.domain.repository

import com.susuaung.navigation.domain.model.CartItem
import com.susuaung.navigation.domain.model.Product
import io.reactivex.Observable

interface ProductRepository {

    fun getPopularProducts(): Observable<List<Product>?>

    fun getSuggestionProducts(): Observable<List<Product>?>

    fun getLatestProducts(): Observable<List<Product>?>

    fun getProductDetailsWithCategory(productId: Int, category: String): Observable<Product?>

    fun addOrUpdateProductToCart(cartItem: CartItem): Observable<Boolean>

    fun getCartItems(userId: String): Observable<List<CartItem>?>

    fun deleteItemFromCart(cartItem: CartItem): Observable<Boolean>
}