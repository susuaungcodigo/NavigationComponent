package com.susuaung.navigation.domain.interactor

import com.codigo.glory.domain.executor.BackgroundThread
import com.codigo.glory.domain.executor.PostExecutionThread
import com.susuaung.navigation.domain.model.result.Result
import com.susuaung.navigation.domain.model.AppUser
import com.susuaung.navigation.domain.repository.UserRepository
import io.reactivex.Observable

class ProfileInteractor constructor(
    private val userRepository: UserRepository,
    mainThread: PostExecutionThread,
    backgroundThread: BackgroundThread
) : BaseInteractor(mainThread, backgroundThread) {

    fun getUser(): Observable<Result<AppUser?>> {
        return Observable.just(Result.Success(userRepository.getUser()))
    }

    fun getProfileImagePath(): Observable<Result<String?>> {
        return Observable.just(Result.Success(userRepository.getProfileImagePath()))
    }
}