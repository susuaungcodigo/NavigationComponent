package com.susuaung.navigation.domain.viewstate.shop

import com.susuaung.navigation.domain.model.AppUser
import com.susuaung.navigation.domain.model.Product

sealed class ProductDetailsPartialState {

    abstract fun reduce(oldState: ProductDetailsViewState): ProductDetailsViewState

    data class ProductDetails(val product: Product?) : ProductDetailsPartialState() {
        override fun reduce(oldState: ProductDetailsViewState): ProductDetailsViewState {
            return oldState.copy(
                product = product,
                loading = false,
                productDetailsError = null,
                addItemToCartResult = false
            )
        }
    }

    data class GetProductDetailsError(val error: Throwable) : ProductDetailsPartialState() {
        override fun reduce(oldState: ProductDetailsViewState): ProductDetailsViewState {
            return oldState.copy(
                product = null,
                loading = false,
                productDetailsError = error,
                addItemToCartResult = false
            )
        }
    }

    object Loading : ProductDetailsPartialState() {
        override fun reduce(oldState: ProductDetailsViewState): ProductDetailsViewState {
            return oldState.copy(
                product = null,
                loading = true,
                productDetailsError = null,
                addItemToCartResult = false
            )
        }
    }

    data class IsLoggedIn(val isLoggedIn: Boolean) : ProductDetailsPartialState() {
        override fun reduce(oldState: ProductDetailsViewState): ProductDetailsViewState {
            return oldState.copy(
                isLoggedIn = isLoggedIn,
                isCheckingDone = true,
                loading = false,
                addItemToCartResult = false
            )
        }
    }

    data class AddItemToCartResult(val isAdded: Boolean) : ProductDetailsPartialState() {
        override fun reduce(oldState: ProductDetailsViewState): ProductDetailsViewState {
            return oldState.copy(
                loading = false,
                addItemToCartResult = isAdded
            )
        }
    }

    data class User(val user: AppUser?) : ProductDetailsPartialState() {
        override fun reduce(oldState: ProductDetailsViewState): ProductDetailsViewState {
            return oldState.copy(
                loading = false,
                user = user,
                isFetchingUserDone = true,
                getUserError = null
            )
        }
    }

    data class GetUserError(val error: Throwable) : ProductDetailsPartialState() {
        override fun reduce(oldState: ProductDetailsViewState): ProductDetailsViewState {
            return oldState.copy(
                loading = false,
                getUserError = error,
                user = null,
                isFetchingUserDone = true
            )
        }
    }
}