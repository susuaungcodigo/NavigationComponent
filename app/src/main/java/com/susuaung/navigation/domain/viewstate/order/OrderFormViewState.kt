package com.susuaung.navigation.domain.viewstate.order

import com.susuaung.navigation.domain.model.AppUser
import com.susuaung.navigation.domain.model.CartItem

data class OrderFormViewState(
    val loadingItems: Boolean = false,
    val orderItems: List<CartItem>? = null,
    val getItemsError: Throwable? = null,
    val loadingUser: Boolean = false,
    val user: AppUser? = null,
    val getUserError: Throwable? = null,
    val isFetchingOrderDone: Boolean = false
)