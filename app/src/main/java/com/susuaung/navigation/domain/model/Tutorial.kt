package com.susuaung.navigation.domain.model

data class Tutorial(
    val title: String,
    val content: String,
    val imgUrl: Int
)