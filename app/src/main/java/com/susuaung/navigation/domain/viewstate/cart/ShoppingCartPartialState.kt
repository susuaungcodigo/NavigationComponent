package com.susuaung.navigation.domain.viewstate.cart

import com.susuaung.navigation.domain.model.CartItem

sealed class ShoppingCartPartialState {

    abstract fun reduce(oldState: ShoppingCartViewState): ShoppingCartViewState

    data class CartItems(val items: List<CartItem>?) : ShoppingCartPartialState() {
        override fun reduce(oldState: ShoppingCartViewState): ShoppingCartViewState {
            return oldState.copy(
                loadingItems = false,
                cartItems = items,
                getItemsError = null,
                removingItem = false,
                itemRemoved = false)
        }
    }

    object LoadingCartItems : ShoppingCartPartialState() {
        override fun reduce(oldState: ShoppingCartViewState): ShoppingCartViewState {
            return oldState.copy(
                loadingItems = true,
                getItemsError = null,
                removingItem = false,
                itemRemoved = false
            )
        }
    }

    data class GetItemsError(val error: Throwable) : ShoppingCartPartialState() {
        override fun reduce(oldState: ShoppingCartViewState): ShoppingCartViewState {
            return oldState.copy(
                getItemsError = error,
                loadingItems = false,
                cartItems = null,
                removingItem = false,
                itemRemoved = false
            )
        }
    }

    data class RemoveItemResult(val removed: Boolean) : ShoppingCartPartialState() {
        override fun reduce(oldState: ShoppingCartViewState): ShoppingCartViewState {
            return oldState.copy(
                itemRemoved = removed,
                removingItem = false,
                removeItemError = null
            )
        }
    }

    object LoadingItemRemove : ShoppingCartPartialState() {
        override fun reduce(oldState: ShoppingCartViewState): ShoppingCartViewState {
            return oldState.copy(
                removingItem = true,
                removeItemError = null,
                itemRemoved = false
            )
        }
    }

    data class RemoveItemError(val error: Throwable) : ShoppingCartPartialState() {
        override fun reduce(oldState: ShoppingCartViewState): ShoppingCartViewState {
            return oldState.copy(
                removingItem = false,
                removeItemError = error,
                itemRemoved = false
            )
        }
    }
}