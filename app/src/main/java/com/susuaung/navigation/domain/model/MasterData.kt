package com.susuaung.navigation.domain.model

class MasterDataModel {
    val master_data: MasterData? = null
    override fun toString(): String {
        return "MasterDataModel(master_data=$master_data)"
    }
}

class MasterData {
    val android: Android? = null
    override fun toString(): String {
        return "MasterData(android=$android)"
    }
}