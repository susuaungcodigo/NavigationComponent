package com.susuaung.navigation.domain.exception

sealed class EmailInputException : Exception() {

    object EmptyEmailException : EmailInputException()

    object InvalidEmailException : EmailInputException()
}

sealed class PasswordInputException : Exception() {

    object PasswordLengthLessThanSixException : PasswordInputException()

    object PasswordIncludesSpaceException : PasswordInputException()

    object EmptyPasswordException : PasswordInputException()
}

sealed class LoginException : Exception() {

    object IncorrectEmailOrPasswordException : LoginException()

    object LoginFailedException : LoginException()
}