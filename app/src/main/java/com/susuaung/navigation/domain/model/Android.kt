package com.susuaung.navigation.domain.model

import java.io.Serializable

class Android : Serializable {
    val maintenance_msg: String = ""
    val latest_version_code: String = ""
    val maintenance_mode: Boolean = false
    val force_update_msg: String = ""
    val force_update_btn: String = ""
    val force_update: Boolean = false
    override fun toString(): String {
        return "MasterData(maintenance_msg='$maintenance_msg'," +
                " latest_version_code='$latest_version_code', " +
                "maintenance_mode=$maintenance_mode, force_update_msg='$force_update_msg'," +
                " force_update_btn='$force_update_btn', force_update=$force_update)"
    }
}