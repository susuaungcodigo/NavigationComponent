package com.susuaung.navigation.domain.viewstate.settings

sealed class SettingsPartialState {
    abstract fun reduce(oldState: SettingsViewState): SettingsViewState


    data class IsLoggedIn(val isLoggedIn: Boolean) : SettingsPartialState() {
        override fun reduce(oldState: SettingsViewState): SettingsViewState {
            return oldState.copy(
                loggedOut = false,
                logoutError = null,
                loggingOut = false,
                isLoggedIn = isLoggedIn
            )
        }
    }

    object LoggedOut : SettingsPartialState() {
        override fun reduce(oldState: SettingsViewState): SettingsViewState {
            return oldState.copy(
                loggedOut = true,
                logoutError = null,
                loggingOut = false,
                isLoggedIn = false
            )
        }
    }

    object LoggingOut : SettingsPartialState() {
        override fun reduce(oldState: SettingsViewState): SettingsViewState {
            return oldState.copy(
                loggedOut = false,
                logoutError = null,
                loggingOut = true
            )
        }
    }

    data class LogoutError(val error: Throwable) : SettingsPartialState() {
        override fun reduce(oldState: SettingsViewState): SettingsViewState {
            return oldState.copy(
                loggedOut = false,
                logoutError = error,
                loggingOut = false
            )
        }
    }

}