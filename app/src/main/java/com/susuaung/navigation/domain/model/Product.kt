package com.susuaung.navigation.domain.model

import java.io.Serializable

data class Product(
    val id: Int = 0,
    val name: String = "",
    val price: Float = 0.0F,
    val imageURL: String? = null,
    val description: String? = null,
    val stock: Int = 0
) : Serializable