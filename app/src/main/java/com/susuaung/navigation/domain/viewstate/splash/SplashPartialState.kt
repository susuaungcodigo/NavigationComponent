package com.susuaung.navigation.domain.viewstate.splash

sealed class SplashPartialState {
    abstract fun reduce(oldState: SplashViewState): SplashViewState

    data class IsFirstTimeAppLaunch(val isFirstTime: Boolean) : SplashPartialState() {
        override fun reduce(oldState: SplashViewState): SplashViewState {
            return oldState.copy(
                user = null,
                getUserError = null,
                isFirstTimeAppLaunch = isFirstTime,
                isFetchingDone = true
            )
        }
    }

    object Loading : SplashPartialState() {
        override fun reduce(oldState: SplashViewState): SplashViewState {
            return oldState.copy(
                getUserError = null,
                loading = true
            )
        }
    }
}