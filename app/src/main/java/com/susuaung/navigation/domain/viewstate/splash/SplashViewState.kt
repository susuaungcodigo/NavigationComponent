package com.susuaung.navigation.domain.viewstate.splash

import com.susuaung.navigation.domain.model.AppUser

data class SplashViewState(
    val user: AppUser? = null,
    val getUserError: Throwable? = null,
    val isFetchingDone: Boolean = false,
    val loading: Boolean = false,
    val isFirstTimeAppLaunch: Boolean = false
)