package com.susuaung.navigation.domain.model

import java.io.Serializable

data class CartItem(
    val userId: String = "",
    val product: Product? = null,
    val count: Int = 0,
    val category: String = ""
) : Serializable