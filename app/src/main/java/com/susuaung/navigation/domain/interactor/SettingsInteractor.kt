package com.susuaung.navigation.domain.interactor

import com.codigo.glory.domain.executor.BackgroundThread
import com.codigo.glory.domain.executor.PostExecutionThread
import com.susuaung.navigation.domain.model.result.Result
import com.susuaung.navigation.domain.repository.UserRepository
import io.reactivex.Observable

class SettingsInteractor constructor(
    private val userRepository: UserRepository,
    mainThread: PostExecutionThread,
    backgroundThread: BackgroundThread
) : BaseInteractor(mainThread, backgroundThread) {

    fun isLoggedIn(): Observable<Result<Boolean>> {
        return Observable.defer {
            val user = userRepository.getUser()
            if (user == null) {
                Observable.just(Result.Success(false))
            } else {
                Observable.just(Result.Success(true))
            }
        }
    }

    fun logout(): Observable<Result<Boolean>> {
        return userRepository.logout().toObservable().map {
            Result.Success(true)
        }
    }
}