package com.susuaung.navigation.domain.model

import java.io.Serializable

data class Ios(
    val force_update: Boolean = false,
    val force_update_btn: String = "",
    val force_update_msg: String = "",
    val latest_version_code: Int = 0,
    val maintenance_mode: Boolean = false,
    val maintenance_msg: String = ""
) : Serializable