package com.susuaung.navigation.domain.model

import java.io.Serializable

data class AppUser(
    val id: String = "",
    var name: String = "",
    var email: String = "",
    var role: String? = "",
    var password: String = ""
) : Serializable