package com.susuaung.navigation.domain.viewstate.shop

import com.susuaung.navigation.domain.model.Product

sealed class ShopPartialState {

    abstract fun reduce(oldState: ShopViewState): ShopViewState


    data class PopularProducts(val products: List<Product>?) : ShopPartialState() {
        override fun reduce(oldState: ShopViewState): ShopViewState {
            return oldState.copy(
                popularProducts = products,
                loadingPopular = false
            )
        }
    }

    data class PopularProductsError(val error: Throwable) : ShopPartialState() {
        override fun reduce(oldState: ShopViewState): ShopViewState {
            return oldState.copy(
                popularProducts = null,
                loadingPopular = false,
                popularProductsError = error
            )
        }
    }

    data class LatestProducts(val products: List<Product>?) : ShopPartialState() {
        override fun reduce(oldState: ShopViewState): ShopViewState {
            return oldState.copy(
                latestProducts = products,
                loadingLatest = false
            )
        }
    }

    data class LatestProductsError(val error: Throwable) : ShopPartialState() {
        override fun reduce(oldState: ShopViewState): ShopViewState {
            return oldState.copy(
                latestProducts = null,
                loadingLatest = false,
                latestProductsError = error
            )
        }
    }

    data class SuggestionProducts(val products: List<Product>?) : ShopPartialState() {
        override fun reduce(oldState: ShopViewState): ShopViewState {
            return oldState.copy(
                suggestionProducts = products,
                loadingSuggestion = false
            )
        }
    }

    data class SuggestionProductsError(val error: Throwable) : ShopPartialState() {
        override fun reduce(oldState: ShopViewState): ShopViewState {
            return oldState.copy(
                suggestionProducts = null,
                loadingSuggestion = false,
                suggestionProductsError = error
            )
        }
    }

    object LoadingPopular : ShopPartialState() {
        override fun reduce(oldState: ShopViewState): ShopViewState {
            return oldState.copy(
                loadingPopular = true
            )
        }
    }

    object LoadingLatest : ShopPartialState() {
        override fun reduce(oldState: ShopViewState): ShopViewState {
            return oldState.copy(
                loadingLatest = true
            )
        }
    }

    object LoadingSuggestion : ShopPartialState() {
        override fun reduce(oldState: ShopViewState): ShopViewState {
            return oldState.copy(
                loadingSuggestion = true
            )
        }
    }
}