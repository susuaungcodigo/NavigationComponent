package com.susuaung.navigation.domain.repository

import com.susuaung.navigation.domain.model.AppUser
import io.reactivex.Observable

interface AuthenticationRepository {
    fun login(appUser: AppUser): Observable<AppUser>
}