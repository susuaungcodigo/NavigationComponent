package com.susuaung.navigation.domain.viewstate.cart

import com.susuaung.navigation.domain.model.CartItem

data class ShoppingCartViewState(
    val loadingItems: Boolean = false,
    val cartItems: List<CartItem>? = null,
    val getItemsError: Throwable? = null,
    val removingItem: Boolean = false,
    val itemRemoved: Boolean = false,
    val removeItemError: Throwable? = null
)