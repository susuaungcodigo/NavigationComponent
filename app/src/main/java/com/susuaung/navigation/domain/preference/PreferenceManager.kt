package com.susuaung.navigation.domain.preference

import com.susuaung.navigation.domain.model.AppUser

interface PreferenceManager {

    fun saveUser(user: AppUser)

    fun getUser(): AppUser?

    fun removeUser()

    fun logout()

    fun saveUserProfilePhoto(path: String)

    fun getUserProfilePath(): String?

    fun saveFirstTimeAppLaunch(isFirstTime: Boolean)

    fun isFirstTimeAppLaunch(): Boolean
}