package com.susuaung.navigation.domain.viewstate.profile

import com.susuaung.navigation.domain.model.AppUser

data class ProfileViewState(
    val userInfo: AppUser? = null,
    val getUserInfoError: Throwable? = null,
    val loading: Boolean = false,
    val profileImagePath: String? = null,
    val isFetchingDone: Boolean = false
)