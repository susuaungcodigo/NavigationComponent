package com.susuaung.navigation.app.feature.order

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.susuaung.navigation.R
import com.susuaung.navigation.domain.model.CountryCode
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_country_code.view.*

class CountryCodesAdapter : RecyclerView.Adapter<CountryCodesAdapter.VHCountryCode>() {

    private var itemList = ArrayList<CountryCode>()
    private val selectSubject = PublishSubject.create<CountryCode>()

    fun countryCodeSelect(): Observable<CountryCode> = selectSubject

    fun setData(items: List<CountryCode>) {
        itemList.clear()
        itemList.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHCountryCode {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_country_code, parent, false)
        return VHCountryCode(view)
    }

    override fun getItemCount(): Int = itemList.size

    override fun onBindViewHolder(holderVH: VHCountryCode, position: Int) {
        holderVH.bind(itemList[position])
        holderVH.itemView.setOnClickListener {
            selectSubject.onNext(itemList[position])
        }
    }

    class VHCountryCode(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(countryCode: CountryCode) {
            itemView.tvCountryCode.text = countryCode.dial_code
            itemView.tvCountryName.text = countryCode.name
        }
    }
}