package com.susuaung.navigation.app.feature.cart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.susuaung.navigation.domain.model.result.Result
import com.codigo.mvi.rx.MviFragment
import com.susuaung.navigation.R
import com.susuaung.navigation.app.feature.home.DrawerActivity
import com.susuaung.navigation.app.utils.CustomAlertDialog
import com.susuaung.navigation.domain.model.CartItem
import com.susuaung.navigation.domain.viewstate.cart.ShoppingCartViewState
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_shopping_cart.*
import org.koin.android.ext.android.inject


class ShoppingCartFragment :
    MviFragment<ShoppingCartViewModel, ShoppingCartViewState, ShoppingCartEvent>() {

    private val shoppingCartViewModel: ShoppingCartViewModel by inject()
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    private lateinit var shoppingCartAdapter: ShoppingCartAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_shopping_cart, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUp()
    }

    override fun onResume() {
        super.onResume()
        (activity as DrawerActivity).toggleCartVisibility(View.GONE)
        shoppingCartViewModel.streamCartItems()
    }

    override fun onStop() {
        super.onStop()
        (activity as DrawerActivity).toggleCartVisibility(View.VISIBLE)
        compositeDisposable.clear()
    }

    private fun setUp() {
        if (!(::shoppingCartAdapter.isInitialized)) {
            shoppingCartAdapter = ShoppingCartAdapter()
        }
        rvCartItem.layoutManager = LinearLayoutManager(context)
        rvCartItem.setHasFixedSize(true)
        rvCartItem.adapter = shoppingCartAdapter

        shoppingCartAdapter.deleteClick().subscribe {
            deleteItemFromCart(it)
        }.addTo(compositeDisposable)

        btnShop.setOnClickListener {
            findNavController().popBackStack()
            findNavController().navigate(R.id.dest_shop)
        }

        btnCheckOut.setOnClickListener {
            getViewModel().getUserForNavigation()
        }
    }

    private fun deleteItemFromCart(cartItem: CartItem) {
        val alertDialog = CustomAlertDialog()
        alertDialog.setParams(
            message = resources.getString(R.string.item_remove_alert),
            positiveAction = ({
                shoppingCartViewModel.removeItemFromCart(cartItem)
            }),
            negativeAction = ({

            })
        )
        alertDialog.show(childFragmentManager, "Remove Item Alert")
    }

    override fun getViewModel(): ShoppingCartViewModel = shoppingCartViewModel

    override fun renderEvent(event: ShoppingCartEvent) {
        when (event) {
            is ShoppingCartEvent.NetworkError ->
                Toast.makeText(context, event.error.localizedMessage, Toast.LENGTH_SHORT).show()
            is ShoppingCartEvent.UserForNavigation -> {
                when (event.userResult) {
                    is Result.Success -> {
                        val direction =
                            ShoppingCartFragmentDirections.actionDestCartToDestOrderForm(event.userResult.data)
                        findNavController().navigate(direction)
                    }
                    is Result.Error -> {
                    } //ignore
                    Result.Loading -> {
                    } //ignore
                }
            }
        }
    }

    override fun render(viewState: ShoppingCartViewState) {
        if (loading(viewState)) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.GONE
        }

        if (viewState.getItemsError != null) {
            Toast.makeText(context, viewState.getItemsError.localizedMessage, Toast.LENGTH_SHORT)
                .show()
        }

        if (viewState.cartItems.isNullOrEmpty() && !loading(viewState)) {
            showEmptyView()
        }

        if (!viewState.cartItems.isNullOrEmpty()) {
            showListView(viewState.cartItems)
        }

        if (viewState.itemRemoved) {
            Toast.makeText(
                context,
                resources.getString(R.string.alert_remove_cart_item_success),
                Toast.LENGTH_SHORT
            )
                .show()
        }
    }

    private fun loading(viewState: ShoppingCartViewState): Boolean {
        return viewState.removingItem || viewState.loadingItems
    }

    private fun showEmptyView() {
        rvCartItem.visibility = View.GONE
        btnCheckOut.visibility = View.GONE
        gpEmpty.visibility = View.VISIBLE
    }

    private fun showListView(items: List<CartItem>) {
        rvCartItem.visibility = View.VISIBLE
        btnCheckOut.visibility = View.VISIBLE
        gpEmpty.visibility = View.GONE
        shoppingCartAdapter.setData(items as ArrayList<CartItem>)
    }
}