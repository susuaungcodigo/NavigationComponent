package com.susuaung.navigation.app.feature.settings

import com.susuaung.navigation.domain.model.result.Result
import com.codigo.mvi.rx.MviViewModel
import com.susuaung.navigation.domain.interactor.SettingsInteractor
import com.susuaung.navigation.domain.viewstate.settings.SettingsPartialState
import com.susuaung.navigation.domain.viewstate.settings.SettingsViewState
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class SettingsViewModel(private val settingsInteractor: SettingsInteractor) :
    MviViewModel<SettingsViewState, SettingsEvent>() {

    private val logoutSubject = PublishSubject.create<Any>()
    private val loggedInCheckSubject = PublishSubject.create<Any>()

    fun logout() {
        logoutSubject.onNext(Any())
    }

    fun checkLoggedInState() {
        loggedInCheckSubject.onNext(Any())
    }

    override fun processIntents(): Observable<SettingsViewState> {
        val logoutState = logoutSubject.switchMap {
            settingsInteractor.logout().startWith(Result.Loading)
        }.map {
            when (it) {
                is Result.Success -> {
                    SettingsPartialState.LoggedOut
                }
                is Result.Error -> {
                    SettingsPartialState.LogoutError(it.exception)
                }
                is Result.Loading -> {
                    SettingsPartialState.LoggingOut
                }
            }
        }

        val checkLoggedInState = loggedInCheckSubject.switchMap {
            settingsInteractor.isLoggedIn().startWith(Result.Loading)
        }.map {
            when (it) {
                is Result.Success -> {
                    SettingsPartialState.IsLoggedIn(it.data)
                }
                is Result.Error -> {
                    SettingsPartialState.IsLoggedIn(false)
                }
                is Result.Loading -> {
                    SettingsPartialState.LoggingOut
                }
            }
        }

        return Observable.mergeArray(
            logoutState, checkLoggedInState
        ).scan(SettingsViewState()) { oldState, partialState ->
            partialState.reduce(oldState)
        }.distinctUntilChanged()
    }
}