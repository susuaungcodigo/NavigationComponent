package com.susuaung.navigation.app.view

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.susuaung.navigation.R
import com.susuaung.navigation.app.GlideApp
import com.susuaung.navigation.domain.model.Product
import kotlinx.android.synthetic.main.item_product.view.*

class VHProduct(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(product: Product) {
        itemView.tvItemName.text = product.name
        itemView.tvItemPrice.text = "$ " + product.price

        if (!product.imageURL.isNullOrEmpty()) {

            val options = RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)

            GlideApp.with(itemView.context)
                .load(product.imageURL)
                .apply(options)
                .into(itemView.ivProduct)
                .onLoadFailed(itemView.context.getDrawable(R.drawable.bg_button_round_corner))
        }
    }
}