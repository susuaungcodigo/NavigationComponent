package com.susuaung.navigation.app.feature.order

import com.susuaung.navigation.domain.model.result.Result
import com.codigo.mvi.rx.MviViewModel
import com.susuaung.navigation.domain.interactor.OrderFormInteractor
import com.susuaung.navigation.domain.viewstate.order.OrderFormPartialState
import com.susuaung.navigation.domain.viewstate.order.OrderFormViewState
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class OrderFormViewModel(private val orderFormInteractor: OrderFormInteractor) :
    MviViewModel<OrderFormViewState, OrderFormEvent>() {

    private val orderItemsSubject = PublishSubject.create<String>()

    fun getOrderItems(userId: String) {
        orderItemsSubject.onNext(userId)
    }

    override fun processIntents(): Observable<OrderFormViewState> {

        val cartItemsState = orderItemsSubject.switchMap {
            orderFormInteractor.getOrderItems(it).startWith(Result.Loading)
        }.map {
            when (it) {
                is Result.Success -> {
                    OrderFormPartialState.OrderItems(it.data)
                }
                is Result.Error -> {
                    OrderFormPartialState.GetItemsError(it.exception)
                }
                is Result.Loading -> {
                    OrderFormPartialState.LoadingOrderItems
                }
            }
        }

        return Observable.mergeArray(
            cartItemsState
        ).scan(OrderFormViewState()) { oldState, partialState ->
            partialState.reduce(oldState)
        }
    }
}