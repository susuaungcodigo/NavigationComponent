package com.susuaung.navigation.app.feature.tutorial

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import com.susuaung.navigation.R
import com.susuaung.navigation.app.feature.home.DrawerActivity
import com.susuaung.navigation.domain.model.Tutorial
import com.susuaung.navigation.domain.preference.PreferenceManager
import kotlinx.android.synthetic.main.fragment_tutorial.*
import org.koin.android.ext.android.inject


class TutorialFragment : DialogFragment() {
    private val prefManager: PreferenceManager by inject()
    private var tutorialList = ArrayList<Tutorial>()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
            STYLE_NORMAL,
            android.R.style.Theme_Black_NoTitleBar_Fullscreen
        )
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar?.hide()
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity).supportActionBar?.show()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tutorial, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
        action()
    }

    private fun setup() {
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                findNavController().popBackStack()
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(this, onBackPressedCallback)

        tutorialList.clear()
        tutorialList.add(
            Tutorial(
                getString(R.string.tutorial_one),
                getString(R.string.content_one),
                R.drawable.ic_tutorial_one
            )
        )
        tutorialList.add(
            Tutorial(
                getString(R.string.tutorial_two),
                getString(R.string.content_two),
                R.drawable.ic_tutorial_two
            )
        )
        tutorialList.add(
            Tutorial(
                getString(R.string.tutorial_three),
                getString(R.string.content_three),
                R.drawable.ic_tutorial_three
            )
        )
        tutorialList.add(
            Tutorial(
                getString(R.string.tutorial_four),
                getString(R.string.content_four),
                R.drawable.ic_tutorial_four
            )
        )

        vpgTutorial.offscreenPageLimit = 4
        vpgTutorial.adapter = TutorialPagerAdapter(context!!, tutorialList)
        indicatorTutorial.setViewPager(vpgTutorial)
    }

    private fun action() {
        txtSkip.setOnClickListener {
            if (prefManager.isFirstTimeAppLaunch()) {
                activity?.finish()
                prefManager.saveFirstTimeAppLaunch(false)
                DrawerActivity.start(
                    context!!,
                    Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                )
                dismiss()
            } else findNavController().popBackStack()
        }

        vpgTutorial.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                if (position == 3) txtSkip.text = getString(R.string.txt_done)
                else txtSkip.text = getString(R.string.skip)
            }
        })
    }
}