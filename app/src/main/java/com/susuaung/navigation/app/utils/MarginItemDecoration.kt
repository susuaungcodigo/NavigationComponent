package com.susuaung.navigation.app.utils

import android.graphics.Rect
import android.view.View

class HorizontalMarginItemDecoration(
    private val verticalMargin: Int,
    private val edgeMargin: Int,
    private val spacing: Int
) : androidx.recyclerview.widget.RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: androidx.recyclerview.widget.RecyclerView,
        state: androidx.recyclerview.widget.RecyclerView.State
    ) {
        with(outRect) {
            if (parent.getChildAdapterPosition(view) == 0) {
                left = edgeMargin
            }
            top = verticalMargin
            bottom = verticalMargin
            right = spacing
            if (parent.getChildAdapterPosition(view) == state.itemCount - 1) {
                right = edgeMargin
            }
        }
    }
}

class CatalogueItemDecoration(
    private val spacingVertical: Int,
    private val spacingHorizontal: Int
) : androidx.recyclerview.widget.RecyclerView.ItemDecoration() {

    private val spanCount = 2

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: androidx.recyclerview.widget.RecyclerView,
        state: androidx.recyclerview.widget.RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view)

        val leftColumn = (position % spanCount) == 0
        if (leftColumn) {
            outRect.right = 0
            outRect.left = 0
        } else {
            outRect.right = 0
            outRect.left = spacingHorizontal
        }
        outRect.bottom = spacingVertical
    }
}