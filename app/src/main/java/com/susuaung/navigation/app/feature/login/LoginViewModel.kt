package com.susuaung.navigation.app.feature.login

import com.susuaung.navigation.domain.model.result.Result
import com.codigo.mvi.rx.MviViewModel
import com.susuaung.navigation.domain.interactor.LoginInteractor
import com.susuaung.navigation.domain.model.AppUser
import com.susuaung.navigation.domain.viewstate.login.LoginPartialState
import com.susuaung.navigation.domain.viewstate.login.LoginViewState
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class LoginViewModel(private val loginInteractor: LoginInteractor) :
    MviViewModel<LoginViewState, LoginEvent>() {

    private val loginSubject = PublishSubject.create<AppUser>()

    fun login(appUser: AppUser) {
        loginSubject.onNext(appUser)
    }

    override fun processIntents(): Observable<LoginViewState> {
        val loginState = loginSubject.switchMap {
            loginInteractor.login(it)
        }.map {
            when (it) {
                is Result.Success -> {
                    LoginPartialState.LoginSuccess(it.data)
                }
                is Result.Error -> {
                    LoginPartialState.LoginError(it.exception)
                }
                Result.Loading -> {
                    LoginPartialState.Loading
                }
            }
        }

        return Observable.mergeArray(
            loginState
        ).scan(LoginViewState()) { oldState, partialState ->
            partialState.reduce(oldState)
        }.distinctUntilChanged()
    }
}