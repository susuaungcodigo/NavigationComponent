package com.susuaung.navigation.app.feature.shop

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.codigo.mvi.rx.MviFragment
import com.susuaung.navigation.R
import com.susuaung.navigation.app.GlideApp
import com.susuaung.navigation.app.utils.CustomAlertDialog
import com.susuaung.navigation.domain.exception.ProductDetailsException
import com.susuaung.navigation.domain.model.AppUser
import com.susuaung.navigation.domain.model.CartItem
import com.susuaung.navigation.domain.model.Product
import com.susuaung.navigation.domain.viewstate.shop.ProductDetailsViewState
import kotlinx.android.synthetic.main.fragment_product_details.*
import org.koin.android.ext.android.inject

class ProductDetailsFragment :
    MviFragment<ProductDetailsViewModel, ProductDetailsViewState, ProductDetailsEvent>() {

    private val productDetailsViewModel: ProductDetailsViewModel by inject()
    private val safeArgs: ProductDetailsFragmentArgs by navArgs()
    private var itemCount: Int = 1
    private var product: Product? = null
    private var appUser: AppUser? = null

    override fun onStart() {
        super.onStart()
        productDetailsViewModel.getUser()
    }

    override fun onResume() {
        super.onResume()
        safeArgs.let {
            Log.d("ProductDetailsFragment", "Product ID : ${it.productId}")
            Log.d("ProductDetailsFragment", "Product Category : ${it.productCategory}")
            productDetailsViewModel.getProductDetailsWithParam(
                it.productId,
                it.productCategory ?: ""
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_product_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUp()
    }

    private fun setUp() {
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                findNavController().navigateUp()
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(this, onBackPressedCallback)

        ivAdd.setOnClickListener {
            if (product != null && itemCount < product?.stock!!) {
                itemCount++
                tvCount.text = itemCount.toString()
            }
        }

        ivRemove.setOnClickListener {
            if (itemCount > 1) {
                itemCount--
                tvCount.text = itemCount.toString()
            }
        }

        btnAddToCart.setOnClickListener {
            if (appUser != null) {
                if (product != null && appUser != null && safeArgs.productCategory != null) {
                    productDetailsViewModel.addItemToCart(
                        CartItem(
                            appUser!!.id,
                            product!!,
                            tvCount.text.toString().toInt(),
                            safeArgs.productCategory!!
                        )
                    )
                }
            } else {
                val alertDialog = CustomAlertDialog()
                alertDialog.setParams(
                    message = resources.getString(R.string.guest_add_to_cart_login_alert),
                    positiveAction = ({
                        findNavController().navigate(R.id.dest_login)
                    }),
                    negativeAction = ({

                    })
                )
                alertDialog.show(childFragmentManager, "Guest Notice Alert")
            }
        }
    }

    override fun getViewModel(): ProductDetailsViewModel = productDetailsViewModel

    override fun renderEvent(event: ProductDetailsEvent) {
        if (event is ProductDetailsEvent.NetworkError)
            Toast.makeText(context, event.error.localizedMessage, Toast.LENGTH_SHORT).show()
    }

    override fun render(viewState: ProductDetailsViewState) {
        if (viewState.loading) {
            pbProductDetails.visibility = View.VISIBLE
        } else {
            pbProductDetails.visibility = View.GONE
        }

        if (viewState.productDetailsError != null) {
           if (viewState.productDetailsError is ProductDetailsException.RetrieveProductDetailsException) {
               showToast(resources.getString(R.string.error_product_details_cannot_retrieve))
           } else {
               showToast(viewState.productDetailsError.localizedMessage)
           }
        }

        if (viewState.product != null) {
            product = viewState.product

            tvProductName.text = product?.name
            tvProductPrice.text = "$ " + product?.price.toString()
            tvProductDescription.text = product?.description
            tvCount.text = "1"
            tvStock.text = resources.getString(R.string.stock_left_template, product?.stock)

            if (!product?.imageURL.isNullOrEmpty()) {
                context?.let { context ->
                    GlideApp.with(context)
                        .load(product?.imageURL)
                        .into(ivProduct)
                        .onLoadFailed(context.getDrawable(R.drawable.bg_button_round_corner))
                }
            }
        }

        if (viewState.addItemToCartResult) {
            Toast.makeText(
                context,
                resources.getString(R.string.alert_add_to_cart_success),
                Toast.LENGTH_SHORT
            ).show()
        }

        if (viewState.isFetchingUserDone) {
            appUser = viewState.user
        }
    }
}