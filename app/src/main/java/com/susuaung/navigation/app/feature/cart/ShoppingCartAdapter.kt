package com.susuaung.navigation.app.feature.cart

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.susuaung.navigation.R
import com.susuaung.navigation.app.GlideApp
import com.susuaung.navigation.domain.model.CartItem
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_cart_item.view.*

class ShoppingCartAdapter : RecyclerView.Adapter<ShoppingCartAdapter.VHShoppingCartItem>() {

    private var itemList = ArrayList<CartItem>()
    private val deleteItemSubject = PublishSubject.create<CartItem>()
    private val increaseSubject = PublishSubject.create<CartItem>()
    private val decreaseSubject = PublishSubject.create<CartItem>()

    fun increaseClick(): Observable<CartItem> = increaseSubject
    fun decreaseClick(): Observable<CartItem> = decreaseSubject //TODO
    fun deleteClick(): Observable<CartItem> = deleteItemSubject

    fun setData(items: List<CartItem>) {
        itemList.clear()
        itemList.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHShoppingCartItem {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_cart_item, parent, false)
        return VHShoppingCartItem(view)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: VHShoppingCartItem, position: Int) {
        holder.bind(itemList[position])
        holder.itemView.ivDelete.setOnClickListener {
            deleteItemSubject.onNext(itemList[position])
        }
    }

    class VHShoppingCartItem(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: CartItem) {

            itemView.tvProductName.text = item.product?.name
            itemView.tvPrice.text = item.product?.price.toString()
            itemView.tvCount.text = item.count.toString()

            val options = RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)

            itemView.context?.let { context ->
                GlideApp.with(context)
                    .load(item.product?.imageURL)
                    .apply(options)
                    .placeholder(context.getDrawable(R.drawable.bg_button_round_corner))
                    .into(itemView.ivProduct)

            }
        }
    }
}

