package com.susuaung.navigation.app.feature.order

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.susuaung.navigation.R
import com.susuaung.navigation.app.GlideApp
import com.susuaung.navigation.domain.model.CartItem
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_order_item.view.*

class OrderItemsAdapter : RecyclerView.Adapter<VHOrderItem>() {

    private var itemList = ArrayList<CartItem>()
    private val increaseSubject = PublishSubject.create<CartItem>()
    private val decreaseSubject = PublishSubject.create<CartItem>()

    fun increaseClick(): Observable<CartItem> = increaseSubject
    fun decreaseClick(): Observable<CartItem> = decreaseSubject

    fun setData(items: List<CartItem>) {
        itemList.clear()
        itemList.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHOrderItem {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_order_item, parent, false)
        return VHOrderItem(view)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holderVH: VHOrderItem, position: Int) {
        holderVH.bind(itemList[position])
    }

}

class VHOrderItem(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(item: CartItem) {

        itemView.tvProductName.text = item.product?.name
        itemView.tvPrice.text = item.product?.price.toString()
        itemView.tvCount.text = item.count.toString()

        itemView.context?.let { context ->

            val options = RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)

            GlideApp.with(context)
                .load(item.product?.imageURL)
                .apply(options)
                .placeholder(context.getDrawable(R.drawable.bg_button_round_corner))
                .into(itemView.ivProduct)

        }
        var subTotal = item.product?.price?.times(item.count)
        itemView.tvSubTotal.text = "$ " + subTotal.toString()
        itemView.tvDeliveryCharges.text = "$ 0.0"
        itemView.tvTotal.text = "$ " + subTotal.toString()
    }
}