package com.susuaung.navigation.app.feature.home

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupWithNavController
import com.google.firebase.database.*
import com.susuaung.navigation.R
import com.susuaung.navigation.app.utils.CustomAlertDialog
import com.susuaung.navigation.domain.model.MasterDataModel
import kotlinx.android.synthetic.main.activity_drawer.*

class DrawerActivity : AppCompatActivity() {

    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration
    private val USER_COLLECTION = "users"
    private lateinit var firebaseDatabase: FirebaseDatabase

    companion object {
        fun start(context: Context, flags: Int) {
            val intent = Intent(context, DrawerActivity::class.java)
            intent.addFlags(flags)
            context.startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        toggleCartVisibility(View.VISIBLE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drawer)

        if (intent != null) {
            val notificationId: String? = intent.getStringExtra("notification_id")
            Log.d("DrawerActivity", "Notification Id: $notificationId")
            if (notificationId != null) {
                val direction =
                    HomeFragmentDirections.actionNotificationToNotificationDetailsFragment()
                direction.notificationId = notificationId
                findNavController(R.id.nav_host_fragment).navigate(direction)
            }
        }

        setSupportActionBar(toolbar)
        navController = nav_host_fragment.findNavController()
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.dest_home,
                R.id.dest_shop,
                R.id.dest_noti,
                R.id.dest_settings,
                R.id.dest_tutorial
            ),
            drawerLayout
        )
        NavigationUI.setupWithNavController(navView, navController)
        toolbar.setupWithNavController(navController, appBarConfiguration)

        getDataFromFirebase()

        ivCart.setOnClickListener {
            findNavController(R.id.nav_host_fragment).navigate(R.id.dest_cart)
        }
    }

    private fun getDataFromFirebase() {
        firebaseDatabase = FirebaseDatabase.getInstance()
        val ref: DatabaseReference = firebaseDatabase.reference
        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                val masterData = dataSnapshot.getValue(MasterDataModel::class.java)
                if (masterData != null) {
                    Log.d("Firebase Database", "MasterData: $masterData")
                    if (masterData.master_data!!.android!!.latest_version_code.toInt() > BuildConfig.VERSION_CODE) {
                        val alertDialog = CustomAlertDialog()
                        alertDialog.setParams(
                            message = masterData.master_data!!.android!!.force_update_msg,
                            positiveAction = ({
                                startActivity(
                                    Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse("market://details?id=com.codigo.fastfast_driver")
                                    )
                                )
                            }),
                            negativeAction = ({

                            })
                        )
                        alertDialog.show(supportFragmentManager, "Updated version available")
                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {

                Log.w("", "loadPost:onCancelled", databaseError.toException())
            }
        }
        ref.addValueEventListener(postListener)
    }

    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.nav_host_fragment).navigateUp(appBarConfiguration)
    }

    fun toggleCartVisibility(visibility: Int) {
        ivCart.visibility = visibility
    }
}