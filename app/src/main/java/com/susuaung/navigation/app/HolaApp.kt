package com.susuaung.navigation.app

import android.app.Application
import com.susuaung.navigation.app.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin

class HolaApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@HolaApp)
            loadKoinModules(
                listOf(appModule, interactorModule, viewModelModule, helperModule, dataModule)
            )
        }
    }
}