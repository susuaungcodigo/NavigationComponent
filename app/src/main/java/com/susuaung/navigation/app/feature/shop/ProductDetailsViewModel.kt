package com.susuaung.navigation.app.feature.shop

import com.susuaung.navigation.domain.model.result.Result
import com.codigo.mvi.rx.MviViewModel
import com.susuaung.navigation.domain.interactor.ProductDetailsInteractor
import com.susuaung.navigation.domain.model.CartItem
import com.susuaung.navigation.domain.viewstate.shop.ProductDetailsPartialState
import com.susuaung.navigation.domain.viewstate.shop.ProductDetailsViewState
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class ProductDetailsViewModel(private val productDetailsInteractor: ProductDetailsInteractor) :
    MviViewModel<ProductDetailsViewState, ProductDetailsEvent>() {

    private val productDetailsWithCategorySubject = PublishSubject.create<Param>()
    private val loggedInCheckSubject = PublishSubject.create<Any>()
    private val addItemToCartSubject = PublishSubject.create<CartItem>()
    private val getUserSubject = PublishSubject.create<Any>()

    fun getProductDetailsWithParam(productId: Int, category: String) {
        productDetailsWithCategorySubject.onNext(Param(productId, category))
    }

    fun checkLoggedInState() {
        loggedInCheckSubject.onNext(Any())
    }

    fun addItemToCart(cartItem: CartItem) {
        addItemToCartSubject.onNext(cartItem)
    }

    fun getUser() {
        getUserSubject.onNext(Any())
    }

    override fun processIntents(): Observable<ProductDetailsViewState> {

        val productDetailsWithCategoryState = productDetailsWithCategorySubject.switchMap {
            productDetailsInteractor.getProductDetailsWithCategory(it.productId, it.category)
                .startWith(Result.Loading)
        }.map {
            when (it) {
                is Result.Success -> {
                    ProductDetailsPartialState.ProductDetails(it.data)
                }
                is Result.Error -> {
                    ProductDetailsPartialState.GetProductDetailsError(it.exception)
                }
                Result.Loading -> {
                    ProductDetailsPartialState.Loading
                }
            }
        }

        val checkLoggedInState = loggedInCheckSubject.switchMap {
            productDetailsInteractor.isLoggedIn().startWith(Result.Loading)
        }.map {
            when (it) {
                is Result.Success -> {
                    ProductDetailsPartialState.IsLoggedIn(it.data)
                }
                is Result.Error -> {
                    ProductDetailsPartialState.IsLoggedIn(false)
                }
                is Result.Loading -> {
                    ProductDetailsPartialState.Loading
                }
            }
        }

        val addItemToCartState = addItemToCartSubject.switchMap {
            productDetailsInteractor.addItemToCart(it).startWith(Result.Loading)
        }.map {
            when (it) {
                is Result.Success -> {
                    ProductDetailsPartialState.AddItemToCartResult(true)
                }
                is Result.Error -> {
                    ProductDetailsPartialState.AddItemToCartResult(false)
                }
                is Result.Loading -> {
                    ProductDetailsPartialState.Loading
                }
            }
        }

        val getUserState = getUserSubject.switchMap {
            productDetailsInteractor.getUser().startWith(Result.Loading)
        }.map {
            when (it) {
                is Result.Success -> {
                    ProductDetailsPartialState.User(it.data)
                }
                is Result.Error -> {
                    ProductDetailsPartialState.GetUserError(it.exception)
                }
                is Result.Loading -> {
                    ProductDetailsPartialState.Loading
                }
            }
        }

        return Observable.mergeArray(
            productDetailsWithCategoryState,
            checkLoggedInState,
            addItemToCartState,
            getUserState
        ).scan(ProductDetailsViewState()) { oldState, partialState ->
            partialState.reduce(oldState)
        }.distinctUntilChanged()
    }

    data class Param(val productId: Int, val category: String)
}