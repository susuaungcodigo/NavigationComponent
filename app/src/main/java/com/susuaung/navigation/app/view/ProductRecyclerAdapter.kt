package com.susuaung.navigation.app.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.susuaung.navigation.R
import com.susuaung.navigation.domain.model.Product
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class ProductRecyclerAdapter : RecyclerView.Adapter<VHProduct>() {

    //private var productList: List<Product> = emptyList()
    private var productList = ArrayList<Product>()
    private val productClickSubject = PublishSubject.create<Product>()

    fun productClick(): Observable<Product> = productClickSubject

    fun setData(newList: ArrayList<Product>) {
        /*val oldList = productList
        productList = newList

        if (oldList.isEmpty()) {
            notifyDataSetChanged()
        } else {
            DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                    val oldItem = oldList[oldItemPosition]
                    val newItem = newList[newItemPosition]
                    return oldItem.id == newItem.id
                }

                override fun getOldListSize(): Int {
                    return oldList.size
                }

                override fun getNewListSize(): Int {
                    return newList.size
                }

                override fun areContentsTheSame(
                    oldItemPosition: Int,
                    newItemPosition: Int
                ): Boolean {
                    val oldItem = oldList[oldItemPosition]
                    val newItem = newList[newItemPosition]
                    return oldItem == newItem
                }
            }).dispatchUpdatesTo(this)
        }*/

        productList.clear()
        productList.addAll(newList)
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHProduct {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false)
        return VHProduct(view)
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    override fun onBindViewHolder(holder: VHProduct, position: Int) {
        holder.bind(productList[position])
        holder.itemView.setOnClickListener {
            productClickSubject.onNext(productList[position])
        }
    }

}