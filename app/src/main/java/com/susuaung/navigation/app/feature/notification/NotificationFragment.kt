package com.susuaung.navigation.app.feature.notification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.susuaung.navigation.R
import com.susuaung.navigation.app.view.NotificationsRecyclerAdapter
import com.susuaung.navigation.domain.model.Notification
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_notifications.*

class NotificationFragment : Fragment() {

    private lateinit var adapter: NotificationsRecyclerAdapter
    private var notifications = ArrayList<Notification>()
    private lateinit var layoutManager: LinearLayoutManager
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_notifications, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvNotifications.isNestedScrollingEnabled = false
        rvNotifications.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(context)
        rvNotifications.layoutManager = layoutManager
        val dividerItemDecoration =
            DividerItemDecoration(rvNotifications.context, layoutManager.orientation)
        rvNotifications.addItemDecoration(dividerItemDecoration)
        adapter = NotificationsRecyclerAdapter()
        rvNotifications.adapter = adapter

        for (i in 1..20) {
            notifications.add(Notification(i, "Title $i", "Message $i"))
        }
        adapter.setData(notifications)

        adapter.detailClicks()
            .subscribe { notification ->
                val direction =
                    NotificationFragmentDirections.actionNotificationFragmentToNotificationDetailsFragment()
                direction.notificationId = notification.id.toString()
                findNavController().navigate(direction)
            }
            .addTo(compositeDisposable)
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }
}