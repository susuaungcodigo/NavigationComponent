package com.susuaung.navigation.app.feature.login

sealed class LoginEvent {
    data class NetworkError(val error: Throwable) : LoginEvent()
}