package com.susuaung.navigation.app.feature.profile

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.navigation.fragment.findNavController
import com.codigo.mvi.rx.MviFragment
import com.susuaung.navigation.R
import com.susuaung.navigation.domain.viewstate.profile.ProfileViewState
import kotlinx.android.synthetic.main.fragment_profile.*
import org.koin.android.ext.android.inject
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class ProfileFragment : MviFragment<ProfileViewModel, ProfileViewState, ProfileEvent>() {

    private val REQUEST_CAMERA_PERMISSION = 1011
    private val REQUEST_READ_EXTERNAL_STORAGE_PERMISSION = 1022
    private val REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION = 1033
    private val REQUEST_IMAGE_PICK = 1044
    private val REQUEST_CAMERA = 1055

    private val profileViewModel: ProfileViewModel by inject()

    private var photoFileUri: Uri? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUp()
    }

    override fun onResume() {
        super.onResume()
        profileViewModel.getUser()
        profileViewModel.getProfileImagePath()
    }

    private fun setUp() {
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                findNavController().navigateUp()
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(this, onBackPressedCallback)

        ivProfile.clipToOutline = true
    }

    override fun getViewModel(): ProfileViewModel = profileViewModel

    override fun renderEvent(event: ProfileEvent) {
        if (event is ProfileEvent.NetworkError)
            Toast.makeText(context, event.error.localizedMessage, Toast.LENGTH_SHORT).show()
    }

    override fun render(viewState: ProfileViewState) {
        if (viewState.loading) {
            flLoading.visibility = View.VISIBLE
        } else {
            flLoading.visibility = View.GONE
        }

        if (viewState.getUserInfoError != null) {
            Toast.makeText(context, viewState.getUserInfoError.localizedMessage, Toast.LENGTH_SHORT)
                .show()
        }

        if (viewState.isFetchingDone) {
            if (viewState.userInfo != null) {
                tvName.text = viewState.userInfo.name
            } else {
                findNavController().navigate(R.id.dest_login)
            }
        }

        if (viewState.profileImagePath != null) {

        }
    }

    private fun showImageOptionsDialog() {
        val colors = context!!.resources.getStringArray(R.array.image_options)
        val builder = AlertDialog.Builder(context!!)
        builder.setItems(colors) { dialog, which ->
            when (which) {
                0 -> { //camera
                    if (!(checkPermission(android.Manifest.permission.CAMERA))) {
                        requestPermission(
                            android.Manifest.permission.CAMERA,
                            REQUEST_CAMERA_PERMISSION
                        )
                    } else {
                        if (ContextCompat.checkSelfPermission(
                                context!!,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                            ) !=
                            PackageManager.PERMISSION_GRANTED
                        ) {
                            requestPermissions(
                                arrayOf(
                                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                                ),
                                REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION
                            )
                        } else {
                            startCameraIntent()
                            //dispatchTakePictureIntent()
                        }
                    }
                }

                1 -> { //gallery
                    if (checkPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE))
                        requestPermission(
                            android.Manifest.permission.READ_EXTERNAL_STORAGE,
                            REQUEST_READ_EXTERNAL_STORAGE_PERMISSION
                        )
                    else {
                        startGalleryIntent()
                    }
                }
            }
            dialog.dismiss()
        }
        builder.show()
    }

    private fun createTempContentUri(context: Context): Uri? =
        try {
            var fileName = System.currentTimeMillis().toString() + ".jpg"
            var values = ContentValues()
            values.put(MediaStore.Images.Media.TITLE, fileName)
            context.contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        } catch (e: Exception) {
            null
        }

    private fun checkPermission(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(
            context!!,
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission(permission: String, requestCode: Int) {
        requestPermissions(arrayOf(permission), requestCode)
    }

    private fun startCameraIntent() {
        val cameraIntent =
            Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        photoFileUri = createTempContentUri(this.context!!)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoFileUri)
        context?.let {
            cameraIntent.resolveActivity(it.packageManager)?.also {
                startActivityForResult(cameraIntent, REQUEST_CAMERA)
            }
        }
    }

    private fun startGalleryIntent() {
        val intent =
            Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.type = "imageURL/png|imageURL/jpg|imageURL/jpeg"
        startActivityForResult(intent, REQUEST_IMAGE_PICK)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CAMERA_PERMISSION -> {
                if (ContextCompat.checkSelfPermission(
                        context!!,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) !=
                    PackageManager.PERMISSION_GRANTED
                ) {
                    requestPermissions(
                        arrayOf(
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE
                        ),
                        REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION
                    )
                } else {
                    startCameraIntent()
                    //dispatchTakePictureIntent()
                }
            }

            REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION -> {
                startCameraIntent()
                //dispatchTakePictureIntent()
            }

            REQUEST_READ_EXTERNAL_STORAGE_PERMISSION -> {
                startGalleryIntent()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_CAMERA -> {
                if (resultCode == Activity.RESULT_OK) {
                    getImageFromCamera()
                }
            }

            REQUEST_IMAGE_PICK -> {
                if (resultCode == Activity.RESULT_OK) {
                    val uri = data?.data
                    //uri?.let { ivProfile.setImageURI(it) }
                    if (uri != null) {
                        getImageFromGallery(uri)
                        photoFileUri = null
                    }
                }
            }
        }
    }

    private fun getImageFromGallery(selectedImage: Uri) {
        val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = context?.contentResolver?.query(
            selectedImage,
            filePathColumn,
            null,
            null,
            null
        )
        cursor?.moveToFirst()
        val columnIndex = cursor?.getColumnIndex(filePathColumn[0])
        val picturePath = columnIndex?.let { cursor.getString(it) } // TODO PATH TO SAVE
        cursor?.close()
        var bitmap = BitmapFactory.decodeFile(picturePath)
        ivProfile.setImageBitmap(bitmap)
    }

    private fun getImageFromCamera() {
        val uri = photoFileUri
        val projection = arrayOf(MediaStore.Images.Media.DATA)
        var cursor = this.context!!.contentResolver.query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            projection, null, null, null
        )
        var columnIndexData = cursor?.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor?.moveToLast()

        var imagePath = columnIndexData?.let { cursor?.getString(it) }
        cursor?.close()
        var bitmap = BitmapFactory.decodeFile(imagePath)
        ivProfile.setImageBitmap(bitmap)
    }

    private var currentPhotoPath: String? = null

    @Throws(IOException::class)
    private fun createImageFile(): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = context!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            currentPhotoPath = absolutePath
        }
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(context!!.packageManager)?.also {

                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    null
                }
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        context!!,
                        "com.susuaung.navigation.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, REQUEST_CAMERA)
                }
            }
        }
    }
}