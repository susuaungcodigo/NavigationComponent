package com.susuaung.navigation.app.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.susuaung.navigation.R
import com.susuaung.navigation.domain.model.Notification
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class NotificationsRecyclerAdapter : RecyclerView.Adapter<VHNotification>() {

    private var notifications = ArrayList<Notification>()

    private val detailSubject = PublishSubject.create<Notification>()

    fun setData(itemList: ArrayList<Notification>) {
        notifications.clear()
        notifications.addAll(itemList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHNotification {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_notification, parent, false)
        return VHNotification(view)
    }

    override fun getItemCount(): Int {
        return notifications.size
    }

    override fun onBindViewHolder(holder: VHNotification, position: Int) {
        holder.bind(notifications[position])
        holder.itemView.setOnClickListener {
            detailSubject.onNext(notifications[position])
        }
    }

    fun detailClicks(): Observable<Notification> = detailSubject
}