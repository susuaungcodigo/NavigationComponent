package com.susuaung.navigation.app.feature.shop

sealed class ProductDetailsEvent {
    data class NetworkError(val error: Throwable) : ProductDetailsEvent()
}