package com.susuaung.navigation.app.feature.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.codigo.mvi.rx.MviFragment
import com.susuaung.navigation.R
import com.susuaung.navigation.app.utils.CustomAlertDialog
import com.susuaung.navigation.domain.viewstate.settings.SettingsViewState
import kotlinx.android.synthetic.main.fragment_settings.*
import org.koin.android.ext.android.inject

class SettingsFragment : MviFragment<SettingsViewModel, SettingsViewState, SettingsEvent>() {

    private val settingsViewModel: SettingsViewModel by inject()

    override fun onStart() {
        super.onStart()
        settingsViewModel.checkLoggedInState()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvProfile.setOnClickListener {
            findNavController().navigate(SettingsFragmentDirections.actionDestSettingsToProfileFragment())
        }

        tvLogin.setOnClickListener {
            findNavController().navigate(R.id.dest_login)
        }

        tvLogout.setOnClickListener {
            val alertDialog = CustomAlertDialog()
            alertDialog.setParams(
                message = resources.getString(R.string.logout_notice),
                positiveAction = ({
                    settingsViewModel.logout()
                }),
                negativeAction = ({

                })
            )
            alertDialog.show(childFragmentManager, "Logout Alert")
        }

        tvNotificationSettings.setOnClickListener {

        }
    }

    override fun getViewModel(): SettingsViewModel = settingsViewModel

    override fun renderEvent(event: SettingsEvent) {
        if (event is SettingsEvent.NetworkError)
            Toast.makeText(context, event.error.localizedMessage, Toast.LENGTH_SHORT).show()
    }

    override fun render(viewState: SettingsViewState) {
        if (viewState.logoutError != null)
            Toast.makeText(
                context,
                viewState.logoutError.localizedMessage,
                Toast.LENGTH_SHORT
            ).show()

        if (viewState.loggedOut) {
            showGuestView()
        }

        if (viewState.isLoggedIn) {
            showUserView()
        } else {
            showGuestView()
        }
    }

    private fun showUserView() {
        gpLoggedIn.visibility = View.VISIBLE
        tvLogout.visibility = View.VISIBLE
        tvLogin.visibility = View.GONE
    }

    private fun showGuestView() {
        gpLoggedIn.visibility = View.GONE
        tvLogout.visibility = View.GONE
        tvLogin.visibility = View.VISIBLE
    }
}