package com.susuaung.navigation.app.di

import com.google.firebase.firestore.FirebaseFirestore
import com.susuaung.navigation.data.preference.PreferenceManagerImpl
import com.susuaung.navigation.data.repository.AuthenticationRepositoryImpl
import com.susuaung.navigation.data.repository.ProductRepositoryImpl
import com.susuaung.navigation.data.repository.UserRepositoryImpl
import com.susuaung.navigation.domain.preference.PreferenceManager
import com.susuaung.navigation.domain.repository.AuthenticationRepository
import com.susuaung.navigation.domain.repository.ProductRepository
import com.susuaung.navigation.domain.repository.UserRepository
import org.koin.dsl.module

var dataModule = module {

    factory { FirebaseFirestore.getInstance() }

    single<PreferenceManager> { PreferenceManagerImpl(get(), get(), "hola") }

    single<AuthenticationRepository> {
        AuthenticationRepositoryImpl(get(), get())
    }

    single<UserRepository> { UserRepositoryImpl(get()) }

    single<ProductRepository> { ProductRepositoryImpl(get()) }
}