package com.susuaung.navigation.app.di

import com.codigo.glory.data.executor.BackgroundThreadImpl
import com.codigo.glory.data.executor.JobsExecutor
import com.codigo.glory.domain.executor.BackgroundThread
import com.codigo.glory.domain.executor.PostExecutionThread
import com.susuaung.navigation.app.UiThread
import com.susuaung.navigation.data.util.SafeGuarder
import org.koin.dsl.module


val appModule = module {
    single { UiThread() as PostExecutionThread }
    single { SafeGuarder(get()) }
    single<BackgroundThread> { BackgroundThreadImpl(JobsExecutor()) }
}