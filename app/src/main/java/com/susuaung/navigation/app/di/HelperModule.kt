package com.susuaung.navigation.app.di

import com.susuaung.navigation.domain.validator.EmailValidator
import com.susuaung.navigation.domain.validator.PasswordValidator
import org.koin.dsl.module

var helperModule = module {

    factory { EmailValidator() }

    factory { PasswordValidator() }
}