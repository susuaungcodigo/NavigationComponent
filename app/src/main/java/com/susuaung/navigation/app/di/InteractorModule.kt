package com.susuaung.navigation.app.di

import com.susuaung.navigation.domain.interactor.*
import org.koin.dsl.module

val interactorModule = module {
    factory { LoginInteractor(get(), get(), get(), get(), get()) }
    factory { ProfileInteractor(get(), get(), get()) }
    factory { SplashInteractor(get(), get(), get()) }
    factory { SettingsInteractor(get(), get(), get()) }
    factory { ProductDetailsInteractor(get(), get(), get(), get()) }
    factory { ShopInteractor(get(), get(), get()) }
    factory { ShoppingCartInteractor(get(), get(), get(), get()) }
    factory { OrderFormInteractor(get(), get(), get(), get()) }
}