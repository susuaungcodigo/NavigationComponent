package com.susuaung.navigation.app.view

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.susuaung.navigation.domain.model.Notification
import kotlinx.android.synthetic.main.item_notification.view.*

class VHNotification(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(notification: Notification) {
        itemView.tvTitle.text = notification.title
        itemView.tvMessage.text = notification.message
    }
}