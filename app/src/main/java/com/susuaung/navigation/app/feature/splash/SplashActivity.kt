package com.susuaung.navigation.app.feature.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.navigation.findNavController
import com.codigo.mvi.rx.MviActivity
import com.google.firebase.iid.FirebaseInstanceId
import com.susuaung.navigation.R
import com.susuaung.navigation.app.feature.home.DrawerActivity
import com.susuaung.navigation.app.feature.home.HomeFragmentDirections
import com.susuaung.navigation.app.feature.tutorial.TutorialFragment
import com.susuaung.navigation.domain.viewstate.splash.SplashViewState
import kotlinx.android.synthetic.main.activity_splash.*
import org.koin.android.ext.android.inject

class SplashActivity : MviActivity<SplashViewModel, SplashViewState, SplashEvent>() {

    private val splashViewModel: SplashViewModel by inject()

    override fun setUpLayout() {
        setContentView(R.layout.activity_splash)
    }

    override fun getViewModel(): SplashViewModel = splashViewModel

    override fun render(viewState: SplashViewState) {
        if (viewState.isFetchingDone) {

            if (viewState.isFirstTimeAppLaunch) {
                Handler().postDelayed({
                    val tutorialFrag = TutorialFragment()
                    tutorialFrag.show(supportFragmentManager, "Tutorial")
                }, 2000)
            } else {
                if (intent != null && intent.getStringExtra("notification_id") != null) {
                    val notificationId: String? = intent.getStringExtra("notification_id")
                    Log.d("SplashActivity", "Notification Id: $notificationId")
                    if (notificationId != null) {
                        val direction =
                            HomeFragmentDirections.actionNotificationToNotificationDetailsFragment()
                        direction.notificationId = notificationId
                        findNavController(R.id.nav_host_fragment).navigate(direction)
                        finish()
                    }
                } else {
                    Handler().postDelayed({
                        DrawerActivity.start(
                            this,
                            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                        )
                        finish()
                    }, 2000)
                }
            }
        }
    }

    override fun renderEvent(event: SplashEvent) {
        if (event is SplashEvent.NetworkError)
            Toast.makeText(this, event.error.localizedMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onStart() {
        super.onStart()
        splashViewModel.isFirstTimeAppLaunch()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        getFirebaseInstanceId()
        ivLeftEye.visibility = View.VISIBLE
        showLayout(tvAppTitle, true)
        Handler().postDelayed({ ivLeftEye.visibility = View.VISIBLE }, 500)
        Handler().postDelayed({ ivRightEye.visibility = View.VISIBLE }, 1000)
        Handler().postDelayed({ ivSmileLip.visibility = View.VISIBLE }, 1500)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent != null && intent.hasExtra("notification_id")) {
            val notificationId: String? = intent.getStringExtra("notification_id")
            Log.d("SplashActivity", "Notification Id: $notificationId")
            if (notificationId != null) {
                val direction =
                    HomeFragmentDirections.actionNotificationToNotificationDetailsFragment()
                direction.notificationId = notificationId
                findNavController(R.id.nav_host_fragment).navigate(direction)
                finish()
            }
        }
    }

    private fun getFirebaseInstanceId() {
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("DrawerActivity", "Get instance id failed", task.exception)
                return@addOnCompleteListener
            }

            val token = task.result?.token
            Log.d("DrawerActivity", "Firebase Token : $token")
        }
    }

    private fun showLayout(view: View, show: Boolean) {
        if (show) {
            view.animate()
                .translationX(0F)
                .setDuration(300)
                .alpha(1F)
                .start()
        } else {
            view.animate()
                .translationX(view.width.toFloat())
                .alpha(0F)
                .setDuration(300)
                .start()
        }
    }
}