package com.susuaung.navigation.app.feature.login

import android.app.Dialog
import android.content.Context.INPUT_METHOD_SERVICE
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.codigo.mvi.rx.MviDialogFragment
import com.susuaung.navigation.R
import com.susuaung.navigation.domain.exception.EmailInputException
import com.susuaung.navigation.domain.exception.LoginException
import com.susuaung.navigation.domain.exception.PasswordInputException
import com.susuaung.navigation.domain.model.AppUser
import com.susuaung.navigation.domain.viewstate.login.LoginViewState
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.ext.android.inject


class LoginFragment : MviDialogFragment<LoginViewModel, LoginViewState, LoginEvent>() {

    override fun getLayoutId(): Int {
        return R.layout.activity_login
    }

    private val loginViewModel: LoginViewModel by inject()

    override fun getViewModel(): LoginViewModel = loginViewModel

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar?.hide()
    }


    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity).supportActionBar?.show()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                findNavController().popBackStack()
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(this, onBackPressedCallback)

        tvLogin.setOnClickListener {
            login()
        }

        edtPassword.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                val imm = context?.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager?
                imm?.hideSoftInputFromWindow(edtPassword.windowToken, 0)
                login()
                true
            }
            false
        }
    }

    override fun renderEvent(event: LoginEvent) {
        if (event is LoginEvent.NetworkError) {
            Toast.makeText(context, event.error.localizedMessage, Toast.LENGTH_SHORT).show()
        }
    }

    override fun render(viewState: LoginViewState) {
        if (viewState.loading) {
            flLoading.visibility = View.VISIBLE
        } else {
            flLoading.visibility = View.GONE
        }

        if (viewState.loginError != null) {
            checkError(viewState.loginError)
        }

        if (viewState.loginSuccess != null) {
            Toast.makeText(
                context,
                resources.getString(R.string.alert_logged_in),
                Toast.LENGTH_SHORT
            ).show()
            findNavController().popBackStack(R.id.dest_home, false)
        }
    }

    private fun checkError(error: Throwable) {
        when (error) {
            is EmailInputException.EmptyEmailException -> {
                showToast(resources.getString(R.string.error_empty_email))
            }
            is EmailInputException.InvalidEmailException -> {
                showToast(resources.getString(R.string.error_invalid_email))
            }
            is PasswordInputException.PasswordLengthLessThanSixException -> {
                showToast(resources.getString(R.string.error_password_length))
            }
            is PasswordInputException.PasswordIncludesSpaceException -> {
                showToast(resources.getString(R.string.error_password_with_space))
            }
            is PasswordInputException.EmptyPasswordException -> {
                showToast(resources.getString(R.string.error_empty_password))
            }
            is LoginException.IncorrectEmailOrPasswordException -> {
                showToast(resources.getString(R.string.error_incorrect_email_password))
            }
            is LoginException.LoginFailedException -> {
                showToast(resources.getString(R.string.error_login_failed))
            }
            else -> showToast(error.localizedMessage)
        }
    }

    private fun login() {
        loginViewModel.login(
            AppUser(
                "1",
                "",
                edtEmail.text.toString(),
                "",
                edtPassword.text.toString()
            )
        )
    }
}