package com.susuaung.navigation.app.feature.order

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.susuaung.navigation.R
import com.susuaung.navigation.app.utils.CommonUtils
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.dialog_country_codes.*

class CountryCodeDialogFragment : DialogFragment() {

    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var countryCodeAdapter: CountryCodesAdapter
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private var countryCodeViewModel: CountryCodeViewModel? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
            STYLE_NORMAL,
            android.R.style.Theme_Material_Light_NoActionBar_Fullscreen
        )
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar?.hide()
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity).supportActionBar?.show()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_country_codes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
        action()
    }

    private fun setup() {

        countryCodeViewModel =
            activity?.let { ViewModelProvider(it).get(CountryCodeViewModel::class.java) }

        val countryCodeList = context?.let { CommonUtils().getCountryCodes(it) }
        rvCountryCodes.isNestedScrollingEnabled = false
        rvCountryCodes.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(context)
        rvCountryCodes.layoutManager = layoutManager
        val dividerItemDecoration =
            DividerItemDecoration(rvCountryCodes.context, layoutManager.orientation)
        rvCountryCodes.addItemDecoration(dividerItemDecoration)
        countryCodeAdapter = CountryCodesAdapter()
        rvCountryCodes.adapter = countryCodeAdapter
        if (countryCodeList != null)
            countryCodeAdapter.setData(countryCodeList.countrycodeList)
    }

    private fun action() {
        countryCodeAdapter.countryCodeSelect().subscribe {
            Log.d("CountryCodeDialog", it.dial_code + " " + it.name)
            countryCodeViewModel?.updateData(it.dial_code)
            this.dismiss()
        }.addTo(compositeDisposable)

        ivClose.setOnClickListener {
            this.dismiss()
        }
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }
}