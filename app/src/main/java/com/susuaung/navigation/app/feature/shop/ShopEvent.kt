package com.susuaung.navigation.app.feature.shop

sealed class ShopEvent {
    data class NetworkError(val error: Throwable) : ShopEvent()
}