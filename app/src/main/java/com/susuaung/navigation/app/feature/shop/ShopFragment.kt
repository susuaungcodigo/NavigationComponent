package com.susuaung.navigation.app.feature.shop

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.codigo.mvi.rx.MviFragment
import com.susuaung.navigation.R
import com.susuaung.navigation.app.utils.CatalogueItemDecoration
import com.susuaung.navigation.app.utils.HorizontalMarginItemDecoration
import com.susuaung.navigation.app.view.ProductRecyclerAdapter
import com.susuaung.navigation.data.util.Constants
import com.susuaung.navigation.domain.model.Product
import com.susuaung.navigation.domain.viewstate.shop.ShopViewState
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_shop.*
import org.koin.android.ext.android.inject

class ShopFragment : MviFragment<ShopViewModel, ShopViewState, ShopEvent>(),
    SwipeRefreshLayout.OnRefreshListener {

    private lateinit var popularProductRecyclerAdapter: ProductRecyclerAdapter
    private lateinit var suggestionsRecyclerAdapter: ProductRecyclerAdapter
    private lateinit var latestProductRecyclerAdapter: ProductRecyclerAdapter

    private val shopViewModel: ShopViewModel by inject()
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_shop, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUp()
    }

    override fun onResume() {
        super.onResume()
        getProducts()
    }

    override fun onRefresh() {
        swipeRefreshLayout.isRefreshing = false
        getProducts()
    }

    private fun getProducts() {
        shopViewModel.getLatestProducts()
        shopViewModel.getPopularProducts()
        shopViewModel.getSuggestionProducts()
    }

    private fun setUp() {

        swipeRefreshLayout.setOnRefreshListener(this)

        if (!(::popularProductRecyclerAdapter.isInitialized))
            popularProductRecyclerAdapter = ProductRecyclerAdapter()
        rvPopularProducts.layoutManager =
            GridLayoutManager(activity, 2, RecyclerView.VERTICAL, false)
        rvPopularProducts.isNestedScrollingEnabled = false
        rvPopularProducts.addItemDecoration(
            CatalogueItemDecoration(
                spacingHorizontal = dpToPx(8f),
                spacingVertical = dpToPx(16f)
            )
        )
        rvPopularProducts.adapter = popularProductRecyclerAdapter
        val animation =
            AnimationUtils.loadLayoutAnimation(activity, R.anim.layout_animation_fall_down)
        rvPopularProducts.layoutAnimation = animation
        rvLatestProducts.setHasFixedSize(true)

        if (!(::suggestionsRecyclerAdapter.isInitialized))
            suggestionsRecyclerAdapter = ProductRecyclerAdapter()
        rvSuggestions.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        rvSuggestions.isNestedScrollingEnabled = false
        rvSuggestions.run {
            addItemDecoration(
                HorizontalMarginItemDecoration(
                    dpToPx(0f),
                    dpToPx(16f),
                    dpToPx(8f)
                )
            )
        }
        rvSuggestions.adapter = suggestionsRecyclerAdapter
        rvLatestProducts.setHasFixedSize(true)

        if (!(::latestProductRecyclerAdapter.isInitialized))
            latestProductRecyclerAdapter = ProductRecyclerAdapter()
        rvLatestProducts.layoutManager =
            GridLayoutManager(activity, 2, RecyclerView.VERTICAL, false)
        rvLatestProducts.isNestedScrollingEnabled = false
        rvLatestProducts.addItemDecoration(
            CatalogueItemDecoration(
                spacingHorizontal = dpToPx(8f),
                spacingVertical = dpToPx(16f)
            )
        )
        rvLatestProducts.adapter = latestProductRecyclerAdapter
        rvLatestProducts.layoutAnimation = animation
        rvLatestProducts.setHasFixedSize(true)

        popularProductRecyclerAdapter.productClick().subscribe {
            goToProductDetails(it, Constants.COLLECTION_POPULAR_PRODUCTS)
        }.addTo(compositeDisposable)

        latestProductRecyclerAdapter.productClick().subscribe {
            goToProductDetails(it, Constants.COLLECTION_LATEST_PRODUCTS)
        }.addTo(compositeDisposable)

        suggestionsRecyclerAdapter.productClick().subscribe {
            goToProductDetails(it, Constants.COLLECTION_SUGGESTION_PRODUCTS)
        }.addTo(compositeDisposable)
    }

    private fun goToProductDetails(product: Product, category: String) {
        val detailsDirection =
            ShopFragmentDirections.actionShopFragmentToProductDetailsFragment()
        detailsDirection.productId = product.id
        detailsDirection.productCategory = category

        //***this currentDestination check is important, if not, after clicking the item first time and come back, the next clicks with give IllegalArgumentException 'navigation destination is unknown to this Navigation Controller'
        //https://stackoverflow.com/questions/54689361/avoiding-android-navigation-illegalargumentexception-in-navcontroller
        //https://stackoverflow.com/questions/51060762/java-lang-illegalargumentexception-navigation-destination-xxx-is-unknown-to-thi
        if (findNavController().currentDestination?.id == R.id.dest_shop) {
            findNavController().navigate(detailsDirection)
        }
    }

    private fun dpToPx(dp: Float): Int {
        val metrics = resources.displayMetrics
        return (dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)).toInt()
    }

    override fun getViewModel(): ShopViewModel = shopViewModel

    override fun renderEvent(event: ShopEvent) {
        if (event is ShopEvent.NetworkError)
            Toast.makeText(context, event.error.localizedMessage, Toast.LENGTH_SHORT).show()
    }

    override fun render(viewState: ShopViewState) {
        if (loading(viewState)) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.GONE
        }

        if (viewState.latestProductsError != null || viewState.popularProductsError != null || viewState.suggestionProductsError != null)
            showToast(resources.getString(R.string.error_product_list_cannot_retrieve))

        if (viewState.latestProducts != null) {
            latestProductRecyclerAdapter.setData(viewState.latestProducts as ArrayList<Product>)
        }

        if (viewState.popularProducts != null) {
            popularProductRecyclerAdapter.setData(viewState.popularProducts as ArrayList<Product>)
        }

        if (viewState.suggestionProducts != null) {
            suggestionsRecyclerAdapter.setData(viewState.suggestionProducts as ArrayList<Product>)
        }
    }

    private fun loading(viewState: ShopViewState): Boolean {
        return viewState.loadingSuggestion || viewState.loadingLatest || viewState.loadingPopular
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }
}
