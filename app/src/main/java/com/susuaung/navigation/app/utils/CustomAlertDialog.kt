package com.susuaung.navigation.app.utils

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.susuaung.navigation.R
import kotlinx.android.synthetic.main.dialog_alert.*

class CustomAlertDialog : DialogFragment() {

    private var alertTitle: String? = null
    private var alertMessage: String? = null
    private lateinit var positiveButtonAction: () -> Unit
    private lateinit var negativeButtonAction: () -> Unit

    fun setParams(
        title: String? = null,
        message: String,
        positiveAction: () -> Unit,
        negativeAction: () -> Unit
    ) {
        alertTitle = title
        alertMessage = message
        positiveButtonAction = positiveAction
        negativeButtonAction = negativeAction
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_alert, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUp()
    }

    private fun setUp() {
        if (alertTitle != null) {
            tvTitle.visibility = View.VISIBLE
            tvTitle.text = alertTitle
        } else {
            tvTitle.visibility = View.GONE
        }
        tvMessage.text = alertMessage

        btnPositive.setOnClickListener {
            this.dismiss()
            positiveButtonAction()
        }

        btnNegative.setOnClickListener {
            this.dismiss()
            negativeButtonAction()
        }
    }

}