package com.susuaung.navigation.app.di

import com.susuaung.navigation.app.feature.cart.ShoppingCartViewModel
import com.susuaung.navigation.app.feature.login.LoginViewModel
import com.susuaung.navigation.app.feature.order.OrderFormViewModel
import com.susuaung.navigation.app.feature.profile.ProfileViewModel
import com.susuaung.navigation.app.feature.settings.SettingsViewModel
import com.susuaung.navigation.app.feature.shop.ProductDetailsViewModel
import com.susuaung.navigation.app.feature.shop.ShopViewModel
import com.susuaung.navigation.app.feature.splash.SplashViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

var viewModelModule = module {
    viewModel { LoginViewModel(get()) }
    viewModel { ProfileViewModel(get()) }
    viewModel { SplashViewModel(get()) }
    viewModel { SettingsViewModel(get()) }
    viewModel { ProductDetailsViewModel(get()) }
    viewModel { ShopViewModel(get()) }
    viewModel { ShoppingCartViewModel(get()) }
    viewModel { OrderFormViewModel(get()) }
}