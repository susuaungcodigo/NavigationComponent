package com.susuaung.navigation.app.feature.profile

sealed class ProfileEvent {
    data class NetworkError(val error: Throwable) : ProfileEvent()
}