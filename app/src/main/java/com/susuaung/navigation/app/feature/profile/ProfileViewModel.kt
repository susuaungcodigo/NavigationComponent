package com.susuaung.navigation.app.feature.profile

import com.susuaung.navigation.domain.model.result.Result
import com.codigo.mvi.rx.MviViewModel
import com.susuaung.navigation.domain.interactor.ProfileInteractor
import com.susuaung.navigation.domain.viewstate.profile.ProfilePartialState
import com.susuaung.navigation.domain.viewstate.profile.ProfileViewState
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class ProfileViewModel(private val profileInteractor: ProfileInteractor) :
    MviViewModel<ProfileViewState, ProfileEvent>() {

    private val userSubject = PublishSubject.create<Any>()
    private val profileImageSubject = PublishSubject.create<Any>()

    fun getUser() {
        userSubject.onNext(Any())
    }

    fun getProfileImagePath() {
        profileImageSubject.onNext(Any())
    }

    override fun processIntents(): Observable<ProfileViewState> {
        val userState = userSubject.switchMap {
            profileInteractor.getUser()
        }.map {
            when (it) {
                is Result.Success -> {
                    ProfilePartialState.UserInfo(it.data)
                }
                is Result.Error -> {
                    ProfilePartialState.GetUserInfoError(it.exception)
                }
                Result.Loading -> {
                    ProfilePartialState.Loading
                }
            }
        }

        val profileImageState = profileImageSubject.switchMap {
            profileInteractor.getProfileImagePath()
        }.map {
            when (it) {
                is Result.Success -> {
                    ProfilePartialState.ProfileImagePath(it.data)
                }
                is Result.Error -> {
                    ProfilePartialState.GetUserInfoError(it.exception)
                }
                Result.Loading -> {
                    ProfilePartialState.Loading
                }
            }
        }

        return Observable.mergeArray(
            userState,
            profileImageState
        ).scan(ProfileViewState()) { oldState, partialState ->
            partialState.reduce(oldState)
        }.distinctUntilChanged()
    }
}