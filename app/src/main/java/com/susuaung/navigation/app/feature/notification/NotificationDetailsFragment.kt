package com.susuaung.navigation.app.feature.notification

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.susuaung.navigation.R
import com.susuaung.navigation.app.feature.home.DrawerActivity
import kotlinx.android.synthetic.main.fragment_notification_details.*

class NotificationDetailsFragment : Fragment() {

    private val safeArgs: NotificationDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_notification_details, container, false)
    }

    override fun onResume() {
        super.onResume()
        (activity as DrawerActivity).toggleCartVisibility(View.GONE)
    }

    override fun onStop() {
        super.onStop()
        (activity as DrawerActivity).toggleCartVisibility(View.VISIBLE)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                //findNavController().navigate(R.id.dest_noti)
                findNavController().navigateUp()
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(this, onBackPressedCallback)

        safeArgs.let {
            tvNotiTitle.text = "Title " + safeArgs.notificationId
            tvNotiContentMessage.text = "Message " + safeArgs.notificationId
        }
    }
}