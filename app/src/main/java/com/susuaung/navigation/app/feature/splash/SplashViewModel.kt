package com.susuaung.navigation.app.feature.splash

import com.codigo.mvi.rx.MviViewModel
import com.susuaung.navigation.domain.interactor.SplashInteractor
import com.susuaung.navigation.domain.viewstate.splash.SplashPartialState
import com.susuaung.navigation.domain.viewstate.splash.SplashViewState
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class SplashViewModel(private val splashInteractor: SplashInteractor) :
    MviViewModel<SplashViewState, SplashEvent>() {

    private val tutorialSubject = PublishSubject.create<Any>()

    fun isFirstTimeAppLaunch() {
        tutorialSubject.onNext(Any())
    }

    override fun processIntents(): Observable<SplashViewState> {

        val tutorialState = tutorialSubject.switchMap {
            splashInteractor.isFirstTimeAppLaunch()
        }.map {
            SplashPartialState.IsFirstTimeAppLaunch(it.data)
        }

        return Observable.mergeArray(
            tutorialState
        ).scan(SplashViewState()) { oldState, partialState ->
            partialState.reduce(oldState)
        }.distinctUntilChanged()
    }

}