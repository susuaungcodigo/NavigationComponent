package com.susuaung.navigation.app.feature.home

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.susuaung.navigation.R
import kotlinx.android.synthetic.main.activity_main.*

class BottomNavigationActivity : AppCompatActivity() {

    private lateinit var navController: NavController

    companion object {
        fun start(context: Context, flags: Int) {
            val intent = Intent(context, BottomNavigationActivity::class.java)
            intent.addFlags(flags)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navController = nav_host_fragment.findNavController()

        //bottom_navigation.setupWithNavController(navController)
        NavigationUI.setupWithNavController(bottom_navigation, navController)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            val dest: String = try {
                resources.getResourceName(destination.id)
            } catch (e: Resources.NotFoundException) {
                destination.id.toString()
            }

            Log.d("NavigationActivity", "Navigated to $dest")
        }
    }

}