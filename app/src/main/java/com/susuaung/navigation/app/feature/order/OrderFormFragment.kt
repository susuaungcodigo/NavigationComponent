package com.susuaung.navigation.app.feature.order

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.codigo.mvi.rx.MviFragment
import com.susuaung.navigation.R
import com.susuaung.navigation.app.feature.home.DrawerActivity
import com.susuaung.navigation.domain.model.AppUser
import com.susuaung.navigation.domain.model.CartItem
import com.susuaung.navigation.domain.viewstate.order.OrderFormViewState
import kotlinx.android.synthetic.main.fragment_order_form.*
import org.koin.android.ext.android.inject

class OrderFormFragment : MviFragment<OrderFormViewModel, OrderFormViewState, OrderFormEvent>() {

    private val orderFormViewModel: OrderFormViewModel by inject()
    private val safeArgs: OrderFormFragmentArgs by navArgs()
    private var appUser: AppUser? = null
    private var countryCodeViewModel: CountryCodeViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_order_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUp()
        action()
    }

    override fun onResume() {
        super.onResume()
        (activity as DrawerActivity).toggleCartVisibility(View.GONE)
    }

    override fun onStop() {
        super.onStop()
        (activity as DrawerActivity).toggleCartVisibility(View.VISIBLE)
    }

    private fun setUp() {
        safeArgs.let {
            appUser = it.user
            if (appUser != null)
                orderFormViewModel.getOrderItems(appUser!!.id)
        }

        countryCodeViewModel =
            activity?.let { ViewModelProvider(it).get(CountryCodeViewModel::class.java) }
        countryCodeViewModel?.getCountryLiveData()
            ?.observe(viewLifecycleOwner, Observer<String> { updatedCountryCode ->
                Log.d("OrderFormFragment", "Selected Country Code : $updatedCountryCode")
                edtCountryCode.setText(updatedCountryCode)
            })

    }

    private fun action() {

        btnPlaceOrder.setOnClickListener {
            //TODO
        }

        edtCountryCode.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                v.clearFocus()
                findNavController().navigate(OrderFormFragmentDirections.actionDestOrderFormToDestCountryCode())
            }
        }

    }

    override fun getViewModel(): OrderFormViewModel = orderFormViewModel

    override fun renderEvent(event: OrderFormEvent) {
        if (event is OrderFormEvent.NetworkError)
            Toast.makeText(context, event.error.localizedMessage, Toast.LENGTH_SHORT).show()
    }

    override fun render(viewState: OrderFormViewState) {
        if (viewState.loadingUser) {
            pbUser.visibility = View.VISIBLE
        } else {
            pbUser.visibility = View.GONE
        }

        if (viewState.loadingItems) {
            pbOrderItems.visibility = View.VISIBLE
        } else {
            pbOrderItems.visibility = View.GONE
        }

        if (viewState.isFetchingOrderDone) {
            if (viewState.orderItems.isNullOrEmpty()) {
                Toast.makeText(
                    context,
                    resources.getString(R.string.alert_order_cannot_retrieve),
                    Toast.LENGTH_SHORT
                ).show()
                findNavController().popBackStack()
            } else {
                showListView(viewState.orderItems)
            }
        }
    }

    private fun showListView(items: List<CartItem>) {
        var total = 0F
        for (item in items) {
            total += (item.count * item.product!!.price)
        }
        tvTotal.text = total.toString()
        //orderItemsAdapter.setData(items)
        //rvOrderItem.adapter = orderItemsAdapter
        //rvOrderItem.visibility = View.VISIBLE
        //btnPlaceOrder.visibility = View.VISIBLE
    }
}