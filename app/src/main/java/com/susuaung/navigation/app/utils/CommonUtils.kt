package com.susuaung.navigation.app.utils

import android.content.Context
import android.net.Uri
import android.provider.OpenableColumns
import android.util.DisplayMetrics
import com.google.gson.Gson
import com.susuaung.navigation.domain.model.CountryCodeList
import java.io.IOException
import java.io.InputStreamReader


class CommonUtils {
    fun dpToPx(context: Context, dp: Float): Int {
        val metrics = context.resources.displayMetrics
        return (dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)).toInt()
    }

    fun getFileNameFromUri(uri: Uri, context: Context): String {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor = context.contentResolver.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } finally {
                cursor!!.close()
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result.substring(cut + 1)
            }
        }
        return result
    }

    fun getExtensionFromFilename(fileName: String): String {
        return fileName.substring(fileName.lastIndexOf(".") + 1)
    }

    fun getCountryCodes(context: Context): CountryCodeList? {
        try {
            val assetManager = context.assets
            val ims = assetManager.open("countrycodes.json")

            val gson = Gson()
            val reader = InputStreamReader(ims)

            val gsonList = gson.fromJson(
                reader, CountryCodeList
                ::class.java
            )
            return gsonList
        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }
    }
}