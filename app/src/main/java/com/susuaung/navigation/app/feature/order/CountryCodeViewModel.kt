package com.susuaung.navigation.app.feature.order

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class CountryCodeViewModel : ViewModel() {
    private val countryCode = MutableLiveData<String>()

    fun getCountryLiveData(): LiveData<String> = countryCode

    fun updateData(data: String) {
        countryCode.value = data
    }
}