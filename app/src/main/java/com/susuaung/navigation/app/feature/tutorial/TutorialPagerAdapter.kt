package com.susuaung.navigation.app.feature.tutorial

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.PagerAdapter
import com.susuaung.navigation.R
import com.susuaung.navigation.domain.model.Tutorial

class TutorialPagerAdapter(private val context: Context, private val tutorialList: List<Tutorial>) :
    PagerAdapter() {
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount() = tutorialList.size

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.item_tutorial, container, false) as ViewGroup
        val title = layout.findViewById<TextView>(R.id.title)
        val imgWalkThrough = layout.findViewById<ImageView>(R.id.imgWalkThrough)
        val txtContent = layout.findViewById<TextView>(R.id.txtContent)
        val bgLayout = layout.findViewById<View>(R.id.mainLayout)

        title.text = tutorialList[position].title
        imgWalkThrough.setImageDrawable(
            ContextCompat.getDrawable(
                context,
                tutorialList[position].imgUrl
            )
        )
        txtContent.text = tutorialList[position].content
        container.addView(layout)
        return layout
    }
}