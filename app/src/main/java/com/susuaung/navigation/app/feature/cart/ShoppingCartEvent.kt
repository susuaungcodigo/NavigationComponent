package com.susuaung.navigation.app.feature.cart

import com.susuaung.navigation.domain.model.result.Result
import com.susuaung.navigation.domain.model.AppUser

sealed class ShoppingCartEvent {
    data class NetworkError(val error: Throwable) : ShoppingCartEvent()

    data class UserForNavigation(val userResult: Result<AppUser>): ShoppingCartEvent()
}