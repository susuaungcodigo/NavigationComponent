package com.susuaung.navigation.app.feature.splash

sealed class SplashEvent {
    data class NetworkError(val error: Throwable) : SplashEvent()
}