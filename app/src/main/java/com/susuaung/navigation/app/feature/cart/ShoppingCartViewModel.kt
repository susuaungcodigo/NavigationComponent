package com.susuaung.navigation.app.feature.cart

import com.susuaung.navigation.domain.model.result.Result
import com.codigo.mvi.rx.MviViewModel
import com.susuaung.navigation.domain.interactor.ShoppingCartInteractor
import com.susuaung.navigation.domain.model.CartItem
import com.susuaung.navigation.domain.viewstate.cart.ShoppingCartPartialState
import com.susuaung.navigation.domain.viewstate.cart.ShoppingCartViewState
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.PublishSubject

class ShoppingCartViewModel(private val shoppingCartInteractor: ShoppingCartInteractor) :
    MviViewModel<ShoppingCartViewState, ShoppingCartEvent>() {

    private val compositeDisposable = CompositeDisposable()
    private val cartItemsSubject = PublishSubject.create<Any>()
    private val removeItemFromCartSubject = PublishSubject.create<CartItem>()


    fun streamCartItems() {
        cartItemsSubject.onNext(Any())
    }

    fun removeItemFromCart(cartItem: CartItem) {
        removeItemFromCartSubject.onNext(cartItem)
    }

    fun getUserForNavigation() {
        shoppingCartInteractor.getUserForNavigation()
            .subscribe({ emitEvent(ShoppingCartEvent.UserForNavigation(it)) }, {})
            .addTo(compositeDisposable)
    }

    override fun processIntents(): Observable<ShoppingCartViewState> {

        val cartItemsState = cartItemsSubject
            .take(1)
            .switchMap {
                shoppingCartInteractor.getCartItems()
                    .startWith(Result.Loading)
            }.map {
                when (it) {
                    is Result.Success -> {
                        ShoppingCartPartialState.CartItems(it.data)
                    }
                    is Result.Error -> {
                        ShoppingCartPartialState.GetItemsError(it.exception)
                    }
                    is Result.Loading -> {
                        ShoppingCartPartialState.LoadingCartItems
                    }
                }
            }

        val removeItemState = removeItemFromCartSubject.switchMap {
            shoppingCartInteractor.removeItemFromCart(it).startWith(Result.Loading)
        }.map {
            when (it) {
                is Result.Success -> {
                    ShoppingCartPartialState.RemoveItemResult(it.data)
                }
                is Result.Error -> {
                    ShoppingCartPartialState.RemoveItemError(it.exception)
                }
                is Result.Loading -> {
                    ShoppingCartPartialState.LoadingItemRemove
                }
            }
        }

        return Observable.mergeArray(
            cartItemsState,
            removeItemState
        ).scan(ShoppingCartViewState()) { oldState, partialState ->
            partialState.reduce(oldState)
        }
    }
}