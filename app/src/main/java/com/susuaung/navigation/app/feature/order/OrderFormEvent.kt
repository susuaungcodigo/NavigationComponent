package com.susuaung.navigation.app.feature.order

sealed class OrderFormEvent {
    data class NetworkError(val error: Throwable) : OrderFormEvent()
}