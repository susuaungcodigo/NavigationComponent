package com.susuaung.navigation.app.feature.shop

import com.susuaung.navigation.domain.model.result.Result
import com.codigo.mvi.rx.MviViewModel
import com.susuaung.navigation.domain.interactor.ShopInteractor
import com.susuaung.navigation.domain.viewstate.shop.ShopPartialState
import com.susuaung.navigation.domain.viewstate.shop.ShopViewState
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class ShopViewModel(private val shopInteractor: ShopInteractor) :
    MviViewModel<ShopViewState, ShopEvent>() {

    private val popularProductsSubject = PublishSubject.create<Any>()
    private val latestProductsSubject = PublishSubject.create<Any>()
    private val suggestionProductsSubject = PublishSubject.create<Any>()

    fun getPopularProducts() {
        popularProductsSubject.onNext(Any())
    }

    fun getLatestProducts() {
        latestProductsSubject.onNext(Any())
    }

    fun getSuggestionProducts() {
        suggestionProductsSubject.onNext(Any())
    }

    override fun processIntents(): Observable<ShopViewState> {

        val popularProductsState = popularProductsSubject.switchMap {
            shopInteractor.getPopularProducts().startWith(Result.Loading)
        }.map {
            when (it) {
                is Result.Success -> {
                    ShopPartialState.PopularProducts(it.data)
                }
                is Result.Error -> {
                    ShopPartialState.PopularProductsError(it.exception)
                }
                Result.Loading -> {
                    ShopPartialState.LoadingPopular
                }
            }
        }

        val latestProductsState = latestProductsSubject.switchMap {
            shopInteractor.getLatestProducts().startWith(Result.Loading)
        }.map {
            when (it) {
                is Result.Success -> {
                    ShopPartialState.LatestProducts(it.data)
                }
                is Result.Error -> {
                    ShopPartialState.LatestProductsError(it.exception)
                }
                Result.Loading -> {
                    ShopPartialState.LoadingLatest
                }
            }
        }

        val suggestionProductsState = suggestionProductsSubject.switchMap {
            shopInteractor.getSuggestionProducts().startWith(Result.Loading)
        }.map {
            when (it) {
                is Result.Success -> {
                    ShopPartialState.SuggestionProducts(it.data)
                }
                is Result.Error -> {
                    ShopPartialState.SuggestionProductsError(it.exception)
                }
                Result.Loading -> {
                    ShopPartialState.LoadingSuggestion
                }
            }
        }

        return Observable.mergeArray(
            popularProductsState,
            latestProductsState,
            suggestionProductsState
        ).scan(ShopViewState()) { oldState, partialState ->
            partialState.reduce(oldState)
        }.distinctUntilChanged()
    }
}