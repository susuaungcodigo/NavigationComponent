package com.susuaung.navigation.app.feature.settings

sealed class SettingsEvent {
    data class NetworkError(val error: Throwable) : SettingsEvent()
}